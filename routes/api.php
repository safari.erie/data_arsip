<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BerandaController;
use App\Http\Controllers\Api\BeritaMobileController;
use App\Http\Controllers\Api\DownloadMobileController;
use App\Http\Controllers\Api\HomeMobileController;
use App\Http\Controllers\Api\InfraDesaMobileController;
use App\Http\Controllers\Api\InfraKecamatanMobileController;
use App\Http\Controllers\Api\InfrastrukturPetaController;
use App\Http\Controllers\Api\InfrastrukturDataController;
use App\Http\Controllers\Api\InfrastrukturDataMobilitasController;
use App\Http\Controllers\Api\InfrastrukturDataAsetController;
use App\Http\Controllers\Api\InfrastrukturDataPPKTController;
use App\Http\Controllers\Api\InfrastrukturDataPondesController;
use App\Http\Controllers\Api\InfrastrukturDataPendudukController;
use App\Http\Controllers\Api\InfrastrukturDataKepegawaianController;
use App\Http\Controllers\Api\InfrastrukturGrafikMobilitasController;
use App\Http\Controllers\Api\InfrastrukturGrafikAsetController;
use App\Http\Controllers\Api\InfrastrukturGrafikPPKTController;
use App\Http\Controllers\Api\InfrastrukturGrafikPondesController;
use App\Http\Controllers\Api\InfrastrukturGrafikPendudukController;
use App\Http\Controllers\Api\InfrastrukturGrafikKepegawaianController;
use App\Http\Controllers\Api\KecamatanKepegawaianMobileController;
use App\Http\Controllers\Api\KecamatanMobileMobilitasController;
use App\Http\Controllers\Api\LoginMobileController;
use App\Http\Controllers\Api\MonevMobileController;
use App\Http\Controllers\Api\ReferenceUtilsMobileController;
use App\Http\Controllers\Back\Konten\BeritaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Home Mobile */

Route::name('mobile.')->prefix('mobile')->group(function () {

    Route::get('home', [HomeMobileController::class, 'Home'])->name('index');
    Route::get('provinces', [ReferenceUtilsMobileController::class, 'UtilProvices'])->name('UtilProvices');
    Route::get('citys', [ReferenceUtilsMobileController::class, 'UtilCitys'])->name('UtilCitys');
    Route::get('districts', [ReferenceUtilsMobileController::class, 'UtilsDistricts'])->name('UtilsDistricts');
    Route::get('villages', [ReferenceUtilsMobileController::class, 'UtilVillages'])->name('UtilVillages');
    Route::get('UtilsJenisKelamins', [ReferenceUtilsMobileController::class, 'UtilsJenisKelamins'])->name('UtilsJenisKelamins');
    Route::get('UtilsYaTidak', [ReferenceUtilsMobileController::class, 'UtilsYaTidak'])->name('UtilsYaTidak');
    Route::get('UtilsKondisis', [ReferenceUtilsMobileController::class, 'UtilsKondisis'])->name('UtilsKondisis');

    Route::post('resetPassword', [ReferenceUtilsMobileController::class, 'resetPassword'])->name('resetPassword');
    Route::post('updateProfileUser', [ReferenceUtilsMobileController::class, 'updateProfileUser'])->name('updateProfileUser');

    /* Infra kecamatan */
    Route::get('infra-kecamatans', [InfraKecamatanMobileController::class, 'GetInfraKecamatans'])->name('GetInfraKecamatans');
    Route::get('/infra-kecamatans-pages', [InfraKecamatanMobileController::class, 'GetInfraKecamatanPages'])->name('GetInfraKecamatanPages');
    Route::get('infra-kecamatan-detil', [InfraKecamatanMobileController::class, 'GetInfraKecamatanDetil'])->name('GetInfraKecamatanDetil');
    Route::post('update-info-kecamatan', [InfraKecamatanMobileController::class, 'UpdateInfoKecamatan'])->name('UpdateInfoKecamatan');
    Route::post('AddKecamatanAsset', [InfraKecamatanMobileController::class, 'AddKecamatanAsset'])->name('AddKecamatanAsset');
    Route::get('GetKecamatanAsetById', [InfraKecamatanMobileController::class, 'GetKecamatanAsetById'])->name('GetKecamatanAsetById');
    Route::get('infra-kecamatan-camat', [InfraKecamatanMobileController::class, 'GetCamat'])->name('GetCamat');
    Route::post('infra-kecamatan-update-camat', [InfraKecamatanMobileController::class, 'UpdateCamat'])->name('UpdateCamat');
    Route::get('infra-kantor-camat', [InfraKecamatanMobileController::class, 'GetKantorCamat'])->name('GetKantorCamat');

    /* infra kecamatan mobilitas */
    Route::post('infra-kecamatan-mobilitas', [KecamatanMobileMobilitasController::class, 'AddKecamatanMobilitas'])->name('AddKecamatanMobilitas');
    Route::post('infra-kecamatan-mobilitas-update', [KecamatanMobileMobilitasController::class, 'UpdateMobilitasKecamatan'])->name('UpdateMobilitasKecamatan');
    Route::get('infra-kecamatan-mobilitas/{id}', [KecamatanMobileMobilitasController::class, 'DataMobilitas'])->name('DataMobilitas');
    Route::get('infra-mobilitas-by-id-mobilitas/', [KecamatanMobileMobilitasController::class, 'getMobilitasById'])->name('getMobilitasById');

    /* infra kecamatan kepegawaian */
    Route::get('infra-kecamatan-kepegawaian/{id}', [KecamatanKepegawaianMobileController::class, 'GetListKepegawaianKecamatan'])->name('GetListKepegawaianKecamatan');
    Route::get('infra-combo-kepegawaian', [KecamatanKepegawaianMobileController::class, 'CombosKepegawaian'])->name('CombosKepegawaian');
    Route::post('infra-kecamatan-kepegawaian-add', [KecamatanKepegawaianMobileController::class, 'AddKepegawaianKec'])->name('AddKepegawaianKec');

    Route::get('kecamatan-asset', [InfraKecamatanMobileController::class, 'KecamatanAset'])->name('KecamatanAset');
    Route::get('infra-desa', [InfraDesaMobileController::class, 'GetInfraDesas'])->name('GetInfraDesas');
    Route::get('infra-desa-detil', [InfraDesaMobileController::class, 'GetInfraDesaDetil'])->name('GetInfraDesaDetil');
    Route::get('infra-desa-kades', [InfraDesaMobileController::class, 'GetKades'])->name('GetKades');
    Route::post('infra-desa-update-kades', [InfraDesaMobileController::class, 'UpdateKades'])->name('UpdateKades');

    Route::post('infra-desa-update-kantor', [InfraDesaMobileController::class, 'UpdateKantorDesa'])->name('UpdateKantorDesa');
    Route::post('infra-desa-update-balai', [InfraDesaMobileController::class, 'UpdateBalai'])->name('UpdateBalai');
    Route::get('infra-desa-balai', [InfraDesaMobileController::class, 'GetBalai'])->name('GetBalai');
    Route::get('infra-camat-balai', [InfraKecamatanMobileController::class, 'GetBalaiCamats'])->name('GetBalaiCamats');
    Route::get('infra-kantor-desa', [InfraDesaMobileController::class, 'GetKantorDesa'])->name('GetKantorDesa');

    /* berita */
    Route::get('beritas', [BeritaMobileController::class, 'GetBeritas'])->name('GetBeritas');
    Route::get('berita', [BeritaMobileController::class, 'GetBerita'])->name('GetBerita');


    /* monev */
    Route::post('/monev/temp', [MonevMobileController::class, 'simpanTemperory'])->name('simpanTemperory');
    Route::post('/monev/saveMonevStep', [MonevMobileController::class, 'saveTemp'])->name('saveTemp');
    Route::get('/monev/Getmonevs', [MonevMobileController::class, 'GetMonevs'])->name('Getmonevs');

    Route::get('/monev/summaryReportMonev', [MonevMobileController::class, 'summaryReportMonev']);
    Route::get('/monev/detailMonevDownload/{monev_id}', [MonevMobileController::class, 'detailReportById']);
    Route::get('/monev/detailMonevById/{monev_id}', [MonevMobileController::class, 'detailMonevById']);
    Route::get('/monev/GetJenisLokpris', [MonevMobileController::class, 'getJenisLokpris']);

    Route::get('/master/GetBobotKondisis', [MonevMobileController::class, 'getBobotKondisis']);
});

Route::resource('/download-dokumen', DownloadMobileController::class)->only(['index', 'store', 'create']);
Route::post('mobile/login', [LoginMobileController::class, 'login'])->name('login');
Route::post('mobile/changePassword', [LoginMobileController::class, 'changePassword'])->name('changePassword');
Route::post('mobile/testPost', [LoginMobileController::class, 'testPost'])->name('testPost');



/** Beranda **/
Route::get('get-grafik', [BerandaController::class, 'getGrafik'])->name('home.grafik');
/** Beranda **/

/** Infrastruktur Data **/
Route::name('data.kecamatan.')->prefix('datatable-kecamatan')->group(function () {
    Route::get('/', [InfrastrukturDataController::class, 'datatableKecamatan'])->name('index');
    Route::get('mobilitas/{id}', [InfrastrukturDataMobilitasController::class, 'datatableKecamatan'])->name('mobilitas');
    Route::get('aset/{id}', [InfrastrukturDataAsetController::class, 'datatableKecamatan'])->name('aset');
    Route::get('ppkt/{id}', [InfrastrukturDataPPKTController::class, 'datatableKecamatan'])->name('ppkt');
    Route::get('penduduk/{id}', [InfrastrukturDataPendudukController::class, 'datatableKecamatan'])->name('penduduk');
    Route::get('kepeg/{id}', [InfrastrukturDataKepegawaianController::class, 'datatableKecamatan'])->name('kepeg');
});

Route::name('data.kelurahan.')->prefix('datatable-kelurahan')->group(function () {
    Route::get('/', [InfrastrukturDataController::class, 'datatableKelurahan'])->name('index');
    Route::get('mobilitas/{id}', [InfrastrukturDataMobilitasController::class, 'datatableKelurahan'])->name('mobilitas');
    Route::get('aset/{id}', [InfrastrukturDataAsetController::class, 'datatableKelurahan'])->name('aset');
    Route::get('pondes/{id}', [InfrastrukturDataPondesController::class, 'datatableKelurahan'])->name('pondes');
    Route::get('penduduk/{id}', [InfrastrukturDataPendudukController::class, 'datatableKelurahan'])->name('penduduk');
    Route::get('kepeg/{id}', [InfrastrukturDataKepegawaianController::class, 'datatableKelurahan'])->name('kepeg');
});
/** End Infrastruktur Data **/

/** Infrastruktur Grafik **/
Route::name('grafik.kecamatan.')->prefix('chart-kecamatan')->group(function () {
    Route::get('mobilitas/{id}', [InfrastrukturGrafikMobilitasController::class, 'chartKecamatan'])->name('mobilitas');
    Route::get('aset/{id}', [InfrastrukturGrafikAsetController::class, 'chartKecamatan'])->name('aset');
    Route::get('ppkt/{id}', [InfrastrukturGrafikPPKTController::class, 'chartKecamatan'])->name('ppkt');
    Route::get('penduduk/{id}', [InfrastrukturGrafikPendudukController::class, 'chartKecamatan'])->name('penduduk');
    Route::get('penduduk-gender/{id}', [InfrastrukturGrafikPendudukController::class, 'chartKecamatanGender'])->name('penduduk.gender');
    Route::get('kepeg/{id}', [InfrastrukturGrafikKepegawaianController::class, 'chartKecamatan'])->name('kepeg');
    Route::get('kepeg-asn/{id}', [InfrastrukturGrafikKepegawaianController::class, 'chartKecamatanAsn'])->name('kepeg.asn');
    Route::get('kepeg-lembaga/{id}', [InfrastrukturGrafikKepegawaianController::class, 'chartKecamatanLembaga'])->name('kepeg.lembaga');
});

Route::name('grafik.kelurahan.')->prefix('chart-kelurahan')->group(function () {
    Route::get('mobilitas/{id}', [InfrastrukturGrafikMobilitasController::class, 'chartKelurahan'])->name('mobilitas');
    Route::get('aset/{id}', [InfrastrukturGrafikAsetController::class, 'chartKelurahan'])->name('aset');
    Route::get('pondes/{id}', [InfrastrukturGrafikPondesController::class, 'chartKelurahan'])->name('pondes');
    Route::get('penduduk/{id}', [InfrastrukturGrafikPendudukController::class, 'chartKelurahan'])->name('penduduk');
    Route::get('penduduk-gender/{id}', [InfrastrukturGrafikPendudukController::class, 'chartKelurahanGender'])->name('penduduk.gender');
    Route::get('kepeg/{id}', [InfrastrukturGrafikKepegawaianController::class, 'chartKelurahan'])->name('kepeg');
    Route::get('kepeg-asn/{id}', [InfrastrukturGrafikKepegawaianController::class, 'chartKelurahanAsn'])->name('kepeg.asn');
    Route::get('kepeg-lembaga/{id}', [InfrastrukturGrafikKepegawaianController::class, 'chartKelurahanLembaga'])->name('kepeg.lembaga');
});
/** End Infrastruktur Data **/

/** Infrastruktur Peta **/
Route::name('map.')->prefix('map')->group(function () {
    Route::get('coords/desa', [InfrastrukturPetaController::class, 'desaCoords'])->name('coord.desa');
    Route::get('coords/kec', [InfrastrukturPetaController::class, 'kecamatanCoords'])->name('coord.kec');
    Route::get('coords/plb', [InfrastrukturPetaController::class, 'plbCoords'])->name('coord.plb');
    Route::get('pin/{tipe}/{id}', [InfrastrukturPetaController::class, 'getDetailTempat'])->name('pin.detail');
});
/** End Infrastruktur Peta **/
