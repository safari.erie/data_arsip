<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Kecamatan\KecamatanAset;
use App\Models\Refactored\Kecamatan\KecamatanCamat;
use App\Models\Refactored\Kecamatan\KecamatanLokpri;
use App\Models\Refactored\Master\AsetItem;
use App\Models\Refactored\Utils\UtilsKecamatan;
use Illuminate\Http\Request;
use App\Traits\KecDetail;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class InfraKecamatanMobileController extends Controller
{
    //
    use KecDetail;

    public function GetInfraKecamatanPages(Request $request)
    {

        $query = DB::table('utils_kecamatan as a')
            ->join('kecamatan_detail as detail', 'a.id', '=', 'detail.id')
            ->join('utils_kota as kota', 'kota.id', '=', 'a.kotaid')
            ->join('utils_provinsi as provinsi', 'kota.provinsiid', '=', 'provinsi.id')
            ->where('a.active', 1)
            ->where('kota.active', 1)
            ->where('provinsi.active', 1);


        if ($idKabkot = $request->input('idKabkot')) {

            $query->where('a.kotaid', '=', $idKabkot);
        }

        $perPage = 0;
        $page = $request->input('perpage');

        if ($page == '' || $page == 0) {

            $perPage = 10;
        } else {
            $perPage = $page;
        }

        $page = $request->input('page', 1);
        $total = $query->count();


        $result =  $query->offset(($page - 1) * $perPage)->limit($perPage)
            ->get(['a.*', 'detail.lokpriid', 'kota.id', 'kota.name as nama_kota', 'provinsi.name as nama_provinsi']);


        if (!empty($result)) {
            $cnt = 1;
            $infraKec = array();
            foreach ($result as $dm) {
                $cur_lokpri = explode(',', $dm->lokpriid);

                $tipe = KecamatanLokpri::select('lokpriid', 'nickname')->whereIn('lokpriid', $cur_lokpri)->get()->toArray();
                $tmp_arr = array(
                    'Rownum'    => $cnt,
                    'Id' => $dm->id,
                    'KotaId' => $dm->kotaid,
                    'Name' => $dm->name,
                    'Tipe' => $tipe,
                    'Kota' => $dm->nama_kota,
                    'Provinsi' => $dm->nama_provinsi,
                    'Active' => $dm->active,
                );
                $cnt++;
                array_push($infraKec, $tmp_arr);
            }
            $responses = array(
                'Status'    => true,
                'Data'      => $infraKec,
                'total' => $total,
                'page' => $page,
                'last_page' => ceil($total / $perPage),
                'Message'   => 'Data Monev'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $responses = array(
                'Status'    => false,
                'Data'      => array(),
                'total' => 0,
                'page' => 0,
                'last_page' => 0,
                'Message'   => 'Not Found'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }

    public function GetInfraKecamatans()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $provs = UtilsKecamatan::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'utils_kecamatan.id',
            'kotaid',
            'utils_kecamatan.name',
            'utils_kecamatan.active',
            'detail.lokpriid', 'kota.name as nama_kota', 'provinsi.name as nama_provinsi'

        ])->join('kecamatan_detail as detail', function ($detail) {
            $detail->on('detail.id', 'utils_kecamatan.id');
        })->join('utils_kota as kota', function ($kota) {
            $kota->on('kota.id', 'utils_kecamatan.kotaid');
        })->join('utils_provinsi as provinsi', function ($prov) {
            $prov->on('provinsi.id', 'kota.provinsiid');
        })
            ->where('utils_kecamatan.active', 1)
            ->whereHas(
                'kota',
                function ($q) {
                    $q->where('active', 1)->whereHas('provinsi', function ($r) {
                        $r->where('active', 1);
                    });
                }
            )
            ->get();


        $infra_kecamatan_arr = array();
        if (count($provs)) {
            foreach ($provs as $prov) {
                $cur_lokpri = explode(',', $prov->lokpriid);

                $tipe = KecamatanLokpri::select('lokpriid', 'nickname')->whereIn('lokpriid', $cur_lokpri)->get()->toArray();
                $tmp_arr = array(
                    'Rownum' => $prov->rownum,
                    'Id' => $prov->id,
                    'KotaId' => $prov->kotaid,
                    'Name' => $prov->name,
                    'Tipe' => $tipe,
                    'Kota' => $prov->nama_kota,
                    'Provinsi' => $prov->nama_provinsi,
                    'Active' => $prov->active,
                );
                $infra_kecamatan_arr[] = $tmp_arr;
            }
            $response_arr = array(
                'Status'    => true,
                'Data'      => $infra_kecamatan_arr,
                'Message'   => 'Data infrastruktur kecamatan  ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => $infra_kecamatan_arr,
                'Message'   => 'Data infrastruktur kecamatan tidak ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
        }

        return $responses;
    }

    public function GetInfraKecamatanDetil(Request $request)
    {
        $kecamatan = UtilsKecamatan::where('id', $request->id)->first();
        if ($kecamatan !== null) {

            $data['kec'] = $kecamatan;
            $data['detail'] = $kecamatan->detail;
            $data['camat'] = $kecamatan->detail->camat;

            switch (@$kecamatan->detail->camat->kondisi_kantor) {
                case 1:
                    $data['camat']['kon_kan'] = 'Baik';
                    break;
                case 2:
                    $data['camat']['kon_kan'] = 'Rusak';
                    break;
                case 0:
                    $data['camat']['kon_kan'] = 'Tidak Ada';
                    break;
                default:
                    $data['camat']['kon_kan'] = 'Tidak Ada';
                    break;
            }
            $data['camat']['sta_kan'] = (@$kecamatan->detail->camat->status_kantor == 1) ? 'Ada' : 'Tidak Ada';

            switch (@$kecamatan->detail->camat->kondisi_balai) {
                case 1:
                    $data['camat']['kon_bal'] = 'Baik';
                    break;
                case 2:
                    $data['camat']['kon_bal'] = 'Rusak';
                    break;
                case 0:
                    $data['camat']['kon_bal'] = 'Tidak Ada';
                    break;
                default:
                    $data['camat']['kon_bal'] = 'Tidak Ada';
                    break;
            }
            $data['camat']['sta_bal'] = (@$kecamatan->detail->camat->status_balai == 1) ? 'Ada' : 'Tidak Ada';

            $data['camat']['foto_kantor'] = (!empty($kecamatan->detail->camat->foto_kantor)) ? asset('upload/camat/kantor') . '/' . $kecamatan->detail->camat->foto_kantor : asset('assets/back/images/preview-3.jpg');
            $data['camat']['foto_balai'] = (!empty($kecamatan->detail->camat->foto_balai)) ? asset('upload/camat/balai') . '/' . $kecamatan->detail->camat->foto_balai : asset('assets/back/images/preview-3.jpg');
            $data['detail'] = $this->getDetailKecamatan($request->id);
            $response_arr = array(
                'Status'    => true,
                'Data'      => $data,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function UpdateInfoKecamatan(Request $request)
    {

        $input = $request->all();
        $valid = Validator::make(
            $input,
            [
                'NamaCamat' => 'required',
                'GenderCamat' => 'required',
                'PendidikanCamat' => 'required'
            ],
            ['required' => ':attribute harus diisi'],
            [
                'NamaCamat' => 'Nama Camat',
                'GenderCamat' => 'Gender Camat',
                'PendidikanCamat' => 'Pendidikan Camat'
            ],
        );

        if (!$valid->fails()) {

            DB::beginTransaction();

            try {
                //code...
                $tableKecamatan = KecamatanCamat::where('id', $request->Id)->first();
                if ($tableKecamatan !== null) {
                    $tableKecamatan->nama_camat = $request->NamaCamat;
                    $tableKecamatan->gender_camat = $request->GenderCamat;
                    $tableKecamatan->pendidikan_camat = $request->PendidikanCamat;

                    $tableKecamatan->status_kantor = $request->StatusKantor;
                    $tableKecamatan->regulasi = $request->Regulasi;
                    $tableKecamatan->kondisi_kantor = $request->KondisiKantor;
                    $tableKecamatan->alamat_kantor = $request->AlamatKantor;
                    $tableKecamatan->kodepos_kantor = $request->KodePosKantor;
                    $tableKecamatan->status_balai = $request->StatusBalai;
                    $tableKecamatan->kondisi_balai = $request->KondisiBalai;

                    $tableKecamatan->save();
                } else {
                    $newTableKecamatan = new KecamatanCamat();
                    $newTableKecamatan->id = $request->Id;
                    $newTableKecamatan->nama_camat = $request->NamaCamat;
                    $newTableKecamatan->gender_camat = $request->GenderCamat;
                    $newTableKecamatan->pendidikan_camat = $request->PendidikanCamat;

                    $newTableKecamatan->status_kantor = $request->StatusKantor;
                    $newTableKecamatan->regulasi = $request->Regulasi;
                    $newTableKecamatan->kondisi_kantor = $request->KondisiKantor;
                    $newTableKecamatan->alamat_kantor = $request->AlamatKantor;
                    $newTableKecamatan->kodepos_kantor = $request->KodePosKantor;

                    $newTableKecamatan->status_balai = $request->StatusBalai;
                    $newTableKecamatan->kondisi_balai = $request->KondisiBalai;
                    $newTableKecamatan->save();
                }

                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Data Berhasil diupdate'
                );


                DB::commit();
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } catch (QueryException $ex) {
                //throw $th;
                DB::rollBack();
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => $valid->errors()->first()
                );

                $responses = response()->json($response_arr,  HttpFoundationResponse::HTTP_OK);
                return $responses;
            }
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => $valid->errors()->first()
            );

            $responses = response()->json($response_arr,  HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function AddKecamatanAsset(Request $request)
    {
        $input = $request->all();

        $valid = Validator::make(
            $input,
            [
                'idKec' => 'required',
                'nama' => 'required',
                'baik' => 'required',
                'rusak' => 'required'
            ],
            [
                'required' => ':attribute harus diisi!',
            ],
            [
                'idKec' => 'Kecamatan',
                'nama' => 'Nama Item',
                'rusak' => 'Jumlah Item Rusak',
                'baik' => 'Jumlah Item Baik'
            ]
        );

        if (!$valid->fails()) {
            $exist = AsetItem::where('nama', $input['nama'])->first();
            $data_master = [
                'iditem' => @$exist->iditem
            ];

            if (empty($exist)) {
                $iditem = $this->getLastIdItem();
                $data_master = [
                    'iditem' => $iditem,
                    'nama' => $input['nama'],
                    'ishapus' => '0',
                ];
            }

            $data_aset = [
                'id' => $input['idKec'],
                'iditem' => $data_master['iditem'],
                'jumlah_baik' => (int)$input['baik'],
                'jumlah_rusak' => (int)$input['rusak']
            ];

            $aset_exist = KecamatanAset::where([['id', $input['idKec']], ['iditem', $data_master['iditem']]])->first();

            if (!empty($aset_exist)) {
                $data_aset['jumlah_baik'] += (int)$aset_exist['jumlah_baik'];
                $data_aset['jumlah_rusak'] += (int)$aset_exist['jumlah_rusak'];
            }

            DB::beginTransaction();
            try {
                if (empty($exist)) {
                    DB::table('aset_item')->insert($data_master);
                }

                if (empty($aset_exist)) {
                    DB::table('kecamatan_aset')->insert($data_aset);
                } else {
                    DB::table('kecamatan_aset')->where([['id', $input['idKec']], ['iditem', $data_master['iditem']]])->update($data_aset);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $e) {
                DB::rollback();
                $oke = false;
                dd($e);
            }

            if ($oke) {
                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Berhasil Menambahkan Aset'
                );
                return response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            } else {
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal Menambahkan Aset'
                );
                return response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            }
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => $valid->errors()->first()
            );
            return response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
        }
    }

    public function KecamatanAset(Request $request)
    {
        $prov = KecamatanAset::select([

            'id',
            'kecamatan_aset.iditem',
            'jumlah_baik',
            'jumlah_rusak',
            'asetItem.nama'
        ])->join('aset_item as asetItem', function ($assetItem) {
            $assetItem->on('asetItem.iditem', 'kecamatan_aset.iditem');
        });
        $provs = $prov->where('id', $request->IdKecamatan)->get();

        if (count($provs) > 0) {
            $response_arr = array(
                'Status' => true,
                'Data'   => $provs,
                'Message'   => 'Data ada'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status' => false,
                'Data'   => array(),
                'Message'   => 'Data ada'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function GetKecamatanAsetById(Request $request)
    {

        $aset = KecamatanAset::where([['id', $request->id], ['iditem', $request->iditem]])->first();
        if (!empty($aset)) {
            $kecamatan = UtilsKecamatan::where('id', $request->id)->first();
            $data['kec'] = $kecamatan;
            $data['aset'] = $aset;
            $response_arr = array(
                'Status'    => false,
                'Data'      => $data,
                'Message'   => 'Data Aset  ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data Aset Tidak ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function GetCamat(Request $request)
    {
        $data = KecamatanCamat::where('id', $request->id)->first();

        if (!empty($data)) {
            $dataCamat = [
                'nama_camat' => @$data->nama_camat,
                'gender_camat' => @$data->gender_camat,
                'pendidikan_camat' => @$data->pendidikan_camat,
            ];

            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataCamat,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function UpdateCamat(Request $request)
    {

        $input = $request->all();
        $valid = Validator::make(
            $input,
            [
                'nama_camat' => 'required',
                'gender_camat' => 'required',
                'pendidikan_camat' => 'required'
            ],
            ['required' => ':attribute harus diisi'],
            [
                'nama_camat' => 'Nama Camat',
                'gender_camat' => 'Gender Camat',
                'pendidikan_camat' => 'Pendidikan Camat'
            ],
        );

        if (!$valid->fails()) {
            $exist = count(KecamatanCamat::where('id', $request->id)->get()->toArray()) > 0;
            $data_camat = [
                'nama_camat' => $input['nama_camat'],
                'gender_camat' => $input['gender_camat'],
                'pendidikan_camat' => $input['pendidikan_camat'],
            ];

            DB::beginTransaction();
            try {
                if ($exist) {
                    DB::table('kecamatan_camat')->where('id', $request->id)->update($data_camat);
                } else {
                    $data_camat['id'] = $request->id;
                    DB::table('kecamatan_camat')->insert($data_camat);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $th) {
                DB::rollback();
                $oke = false;
                dd($th);
            }

            if ($oke) {
                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Berhasil mengubah Informasi Camat'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } else {
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal mengubah Informasi Kepala Camat'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
                return $responses;
            }
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => $valid->errors()->first()
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function GetKantorCamat(Request $request)
    {
        $data = KecamatanCamat::where('id', $request->id)->first();
        if (!empty($data)) {
            $dataKantorCamat = [
                'status' => @$data->status_kantor,
                'alamat' => @$data->alamat_kantor,
                'kondisi' => @$data->kondisi_kantor,
                'regulasi' => @$data->regulasi,
            ];
            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataKantorCamat,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function GetBalaiCamats(Request $request)
    {

        $data = KecamatanCamat::where('id', $request->id)->first();
        $dataBalaiCamat = [
            'status' => @$data->status_balai,
            'kondisi' => @$data->kondisi_balai,
        ];

        if (!empty($dataBalaiCamat)) {
            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataBalaiCamat,
                'Message'   => 'Data ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => true,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    private function getLastIdItem()
    {
        $last = AsetItem::max('iditem');
        return !empty($last) ? $last + 1 : 1;
    }
}
