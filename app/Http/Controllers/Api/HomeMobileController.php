<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Kecamatan\KecamatanDetail;
use App\Models\Refactored\Konten\Content;
use App\Models\Refactored\Utils\UtilsDesa;
use App\Models\Refactored\Utils\UtilsKecamatan;
use App\Models\Refactored\Utils\UtilsKota;
use App\Models\Refactored\Utils\UtilsProvinsi;
use App\Models\Settings\ApplSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class HomeMobileController extends Controller
{
    //
    const BANNER =    'banner';
    public function Home()
    {
        $dataBanners = array();
        $getBanners = ApplSetting::where('code_type', $this::BANNER)->get();
        if (!empty($getBanners)) {
            foreach ($getBanners as $getBanner) {
                $tmpArray = array(
                    'codeType'  => $getBanner->code_type,
                    'fileGambar'    => (!empty($getBanner->value_string)) ? asset('/upload/banner') . "/" . $getBanner->value_string : ''
                );

                $dataBanners[] = $tmpArray;
            }
        }

        $dataBeritas = array();

        $tmpBeritas = array(
            [
                "IdBerita"  => '1',
                "NamaBerita"    => 'Peternak di Sonis Laloran Awetkan Pakan Sapi Untuk Persediaan Musim Kemarau',
                "ImageBerita"   => "http://datasarprasip.bnpp.go.id/upload/content/tangkal-covid19-mppi-bagikan-apd-pada-masyarakat-perbatasan-distrik-ninati-boven-digoel-papua1633576474/poster-1633576474.jpeg",
                "date"          => '21 Nov 2020'
            ],
            [
                "IdBerita"  => '2',
                "NamaBerita"    => 'BNPP Gandeng Detik.com Tingkatkan Publikasi Tentang Perbatasan Negara',
                "ImageBerita"   => "http://datasarprasip.bnpp.go.id/upload/content/persiapan-hadapi-musim-utara-nelayan-di-kabupaten-bintan-dilatih-menanam-sayuran-hidroponik1605901985/poster-1605901985.jpeg",
                "date"          => '21 Nov 2020'
            ],
            [
                "IdBerita"  => '2',
                "NamaBerita"    => 'BNPP Gandeng Detik.com Tingkatkan Publikasi Tentang Perbatasan Negara',
                "ImageBerita"   => "http://datasarprasip.bnpp.go.id/upload/content/masyarakat-perbatasan-jadi-bagian-sistem-pertahanan-dan-keamanan-di-batas-negeri1605901925/poster-1605901925.jpeg",
                "date"          => '21 Nov 2020'
            ],

        );

        $beritas = Content::where('status', 1)->orderBy('idcontent', 'desc')->limit(5)->get();

        $beritaArr = array();
        if (!empty($beritas)) {
            foreach ($beritas as $berita) {
                $myBerita = array(
                    'id'    => $berita->idcontent,
                    'judul' => $berita->judul,
                    'fileImage' => asset('upload/content/' . $berita->slug . '/' . $berita->poster),
                    // 'created' => $berita->created_at != null ? date_format($berita->created_at, "Y-m-d") : '',
                    'created' => $berita->created_at != null ? date('d-m-Y', strtotime($berita->created_at)) : '',
                );
                $beritaArr[] = $myBerita;
            }
        }



        $countProvinceActive = UtilsProvinsi::where('active', 1)->count();
        $countKotaActive = UtilsKota::where('active', 1)->count();
        $countKecamatanActive = UtilsKecamatan::where('active', 1)->count();
        $countDesaActive = UtilsDesa::where('active', 1)->count();
        $countLokrpi = KecamatanDetail::where('lokpriid', 'like', '1%')->count();

        $tmpCountRegion = array(
            [
                "LabelTitleProvinsi"    => 'Provinsi',
                "provinsi"  => $countProvinceActive
            ],
            [
                "LabelTitleKota"    => 'Kota',
                "Kota"  => $countKotaActive
            ],
            [
                "LabelTitleKecamatan"    => 'Kecamatan',
                "kecamatan"  => $countKecamatanActive
            ],
            [
                "LabelTitleDesa"    => 'Desa',
                "Desa"  => $countDesaActive
            ],
            [
                "LabelTitleLokpri"    => 'Lokpri',
                "Lokpri"  => $countLokrpi
            ],

        );

        $dataGrafik = $this->getGrafik();

        $dataBeritas['banners'] = $dataBanners;
        $dataBeritas['beritas'] = $tmpBeritas;
        $dataBeritas['dataBeritas'] = $beritaArr;
        $dataBeritas['countWilayah'] = $tmpCountRegion;
        $dataBeritas['graph'] = $dataGrafik;

        $dataHome = $dataBeritas;
        $responses = array(
            'Status'    => true,
            'Data'      => $dataHome,
            'Message'   => 'Data Home'
        );
        $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
        return $responses;
    }

    private function getGrafik()
    {
        $query = "SELECT p.name, COALESCE(a.jumlah,0) AS lokpri, COALESCE(b.jumlah,0) AS pksn, COALESCE(c.jumlah,0) AS ppkt FROM utils_provinsi p
                    LEFT JOIN(
                        SELECT COUNT(aa.id) AS jumlah, ac.id, ac.provinsiid FROM utils_kecamatan aa
                        INNER JOIN utils_kota ac ON aa.kotaid = ac.id
                        LEFT JOIN kecamatan_detail ab ON aa.id = ab.id
                        WHERE aa.active = 1 AND ab.lokpriid LIKE '%1%'
                        AND ac.active = 1
                        GROUP BY ac.provinsiid
                    ) a ON a.provinsiid = p.id
                    LEFT JOIN (
                        SELECT COUNT(aa.id) AS jumlah, ac.id, ac.provinsiid FROM utils_kecamatan aa
                        INNER JOIN utils_kota ac ON aa.kotaid = ac.id
                        LEFT JOIN kecamatan_detail ab ON aa.id = ab.id
                        WHERE aa.active = 1 AND ab.lokpriid LIKE '%2%'
                        AND ac.active = 1
                        GROUP BY ac.provinsiid
                    ) b ON b.provinsiid = p.id
                    LEFT JOIN (
                        SELECT COUNT(aa.id) AS jumlah, ac.id, ac.provinsiid FROM utils_kecamatan aa
                        INNER JOIN utils_kota ac ON aa.kotaid = ac.id
                        LEFT JOIN kecamatan_detail ab ON aa.id = ab.id
                        WHERE aa.active = 1 AND ab.lokpriid LIKE '%3%'
                        AND ac.active = 1
                        GROUP BY ac.provinsiid
                    ) c ON c.provinsiid = p.id
                    WHERE p.active = 1";
        $res = DB::select(DB::raw($query));

        $data = [
            'label' => [],
            'lokpri' => [],
            'pksn' => [],
            'ppkt' => [],
        ];

        foreach ($res as $key => $value) {
            array_push($data['label'], $value->name);
            array_push($data['lokpri'], $value->lokpri);
            array_push($data['pksn'], $value->pksn);
            array_push($data['ppkt'], $value->ppkt);
        }

        return $data;
    }
}
