<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Desa\DesaKades;
use App\Models\Refactored\Utils\UtilsDesa;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class InfraDesaMobileController extends Controller
{
    //
    public function GetInfraDesas()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $desas = UtilsDesa::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'utils_desa.id',
            'kecamatanid',
            'utils_desa.name',
            'kecamatan.name as nama_kecamatan',
            'kota.name as nama_kota',
            'utils_desa.active'

        ])
            ->join('utils_kecamatan as kecamatan', function ($kecamatan) {
                $kecamatan->on('kecamatan.id', 'utils_desa.kecamatanid');
            })
            ->join('utils_kota as kota', function ($kota) {
                $kota->on('kota.id', 'kecamatan.kotaid');
            })->join('utils_provinsi as provinsi', function ($prov) {
                $prov->on('provinsi.id', 'kota.provinsiid');
            })
            ->where('utils_desa.active', '1')->get();


        $response_arr = array(
            'Status'    => true,
            'Data'      => $desas,
            'Message'   => 'Data ditemukan'
        );

        return $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
    }

    public function GetInfraDesaDetil(Request $request)
    {
        $desa = UtilsDesa::where('id', $request->id)->first();
        if ($desa !== null) {

            $data['kec'] = $desa;
            $data['detail'] = @$desa->detail;
            $data['kades'] = @$desa->detail->kades;
            switch (@$desa->detail->kades->kondisi_kantor) {
                case 1:
                    $data['kades']['kon_kan'] = 'Baik';
                    break;
                case 2:
                    $data['kades']['kon_kan'] = 'Rusak';
                    break;
                case 0:
                    $data['kades']['kon_kan'] = 'Tidak Ada';
                    break;
                default:
                    $data['kades']['kon_kan'] = 'Tidak Ada';
                    break;
            }

            $data['camat']['sta_kan'] = (@$desa->detail->kades->status_kantor == 1) ? 'Ada' : 'Tidak Ada';

            switch (@$desa->detail->kades->kondisi_balai) {
                case 1:
                    $data['kades']['kon_bal'] = 'Baik';
                    break;
                case 2:
                    $data['kades']['kon_bal'] = 'Rusak';
                    break;
                case 0:
                    $data['kades']['kon_bal'] = 'Tidak Ada';
                    break;
                default:
                    $data['kades']['kon_bal'] = 'Tidak Ada';
                    break;
            }
            $data['kades']['sta_bal'] = (@$desa->detail->kades->status_balai == 1) ? 'Ada' : 'Tidak Ada';

            $data['kades']['foto_kantor'] = (!empty($desa->detail->kades->foto_kantor)) ? asset('upload/desa/kantor') . '/' . $desa->detail->kades->foto_kantor : asset('assets/back/images/preview-3.jpg');
            $data['kades']['foto_balai'] = (!empty($desa->detail->kades->foto_balai)) ? asset('upload/desa/balai') . '/' . $desa->detail->kades->foto_balai : asset('assets/back/images/preview-3.jpg');
            //$data['detail'] = $this->getDetailKecamatan($request->id);
            $response_arr = array(
                'Status'    => true,
                'Data'      => $data,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function GetKades(Request $request)
    {
        $data = DesaKades::where('id', $request->id)->first();
        if (!empty($data)) {
            $dataKades = [
                'nama_kades' => @$data->nama_kades,
                'gender_kades' => @$data->gender_kades,
                'pendidikan_kades' => @$data->pendidikan_kades,
            ];
            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataKades,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function UpdateKades(Request $request)
    {
        $input = $request->all();

        $valid = Validator::make(
            $input,
            [
                'nama_kades' => 'required',
                'gender_kades' => 'required',
                'pendidikan_kades' => 'required'
            ],
            ['required' => ':attribute harus diisi'],
            [
                'nama_kades' => 'Nama Kades',
                'gender_kades' => 'Gender Kades',
                'pendidikan_kades' => 'Pendidikan Kades'
            ],
        );

        if (!$valid->fails()) {
            $exist = count(DesaKades::where('id', $request->id)->get()->toArray()) > 0;
            $data_camat = [
                'nama_kades' => $input['nama_kades'],
                'gender_kades' => $input['gender_kades'],
                'pendidikan_kades' => $input['pendidikan_kades'],
            ];

            DB::beginTransaction();
            try {
                if ($exist) {
                    DB::table('desa_kades')->where('id', $request->id)->update($data_camat);
                } else {
                    $data_camat['id'] = $request->id;
                    DB::table('desa_kades')->insert($data_camat);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $th) {
                DB::rollback();
                $oke = false;
                dd($th);
            }

            if ($oke) {

                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Berhasil mengubah Informasi Kepala Desa'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } else {
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal mengubah Informasi Kepala Desa'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
                return $responses;
            }
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => $valid->errors()->first()
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function GetKantorDesa(Request $request)
    {
        $data = DesaKades::where('id', $request->id)->first();
        if (!empty($data)) {
            $dataKantorKades = [
                'status' => @$data->status_kantor,
                'alamat' => @$data->alamat_desa,
                'kondisi' => @$data->kondisi_kantor,
                'regulasi' => @$data->regulasi,
            ];
            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataKantorKades,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function UpdateKantorDesa(Request $request)
    {
        $input = $request->all();

        $valid = Validator::make(
            $input,
            [
                'status' => 'required',
                //  'foto' => 'mimes:png,jpg,jpeg|max:2048'
            ],
            /*  [
                'required' => ':attribute harus diisi',
                'mimes' => 'format :attribute harus :mimes',
                'foto:max' => 'Ukuran maksimal foto adalah :max'
            ], */
            [
                'status' => 'Status Kantor',
                'alamat' => 'Alamat Kantor',
                'kondisi' => 'Kondisi Kantor',
                'regulasi' => 'Regulasi',
                'foto' => 'Foto Kantor'
            ],
        );

        $valid->sometimes(['alamat', 'kondisi'], 'required', function () use ($input) {
            return $input['status'] == 1;
        });

        if (!$valid->fails()) {
            $exist = count(DesaKades::where('id', $request->id)->get()->toArray()) > 0;
            $data_kantor = [
                'status_kantor' => $input['status'],
                'alamat_desa' => ($input['status'] == 0) ? '' : $input['alamat'],
                'kondisi_kantor' => ($input['status'] == 0) ? '0' : $input['kondisi'],
                'regulasi' => $input['regulasi'],
            ];

            /* if ($request->has('foto')) {
                $imagename = strtolower('kantor-desa' . preg_replace('/\s+/', '', UtilsDesa::where('id', $request->id)->first()->name) . '-' . time() . '.' . $request->foto->extension());
                $request->foto->move(public_path('upload/desa/kantor'), $imagename);
                $data_kantor['foto_kantor'] = $imagename;
            } */

            DB::beginTransaction();
            try {
                if ($exist) {
                    DB::table('desa_kades')->where('id', $request->id)->update($data_kantor);
                } else {
                    $data_kantor['id'] = $request->id;
                    DB::table('desa_kades')->insert($data_kantor);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $th) {
                DB::rollback();
                $oke = false;
            }

            if ($oke) {

                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Berhasil mengubah Informasi Kantor Desa'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);


                return $responses;
            } else {

                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal mengubah Informasi Kantor Desa'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);


                return $responses;
            }
        } else {

            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   =>  $valid->errors()->first()
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);


            return $responses;
        }
    }


    public function GetBalai(Request $request)
    {
        $data = DesaKades::where('id', $request->id)->first();
        if (!empty($data)) {
            $dataBalai = [
                'status' => @$data->status_balai,
                'kondisi' => @$data->kondisi_balai,
            ];
            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataBalai,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function UpdateBalai(Request $request)
    {
        $input = $request->all();

        $valid = Validator::make(
            $input,
            [
                'status' => 'required',
                // 'foto' => 'mimes:png,jpg,jpeg|max:2048'
            ],
            /* [
                'required' => ':attribute harus diisi',
                'mimes' => 'format :attribute harus :mimes',
                'foto:max' => 'Ukuran maksimal foto adalah :max'
            ], */
            [
                'status' => 'Status Balai',
                'kondisi' => 'Kondisi Balai',
                //'foto' => 'Foto Balai'
            ],
        );

        $valid->sometimes('kondisi', 'required', function () use ($input) {
            return $input['status'] == 1;
        });

        if (!$valid->fails()) {
            $exist = count(DesaKades::where('id', $request->id)->get()->toArray()) > 0;
            $data_balai = [
                'status_balai' => $input['status'],
                'kondisi_balai' => ($input['status'] == 0) ? '0' : $input['kondisi'],
            ];

            /* if ($request->has('foto')) {
                $imagename = strtolower('balai-desa' . preg_replace('/\s+/', '', UtilsDesa::where('id', $id)->first()->name) . '-' . time() . '.' . $request->foto->extension());
                $request->foto->move(public_path('upload/desa/balai'), $imagename);
                $data_balai['foto_balai'] = $imagename;
            } */

            DB::beginTransaction();
            try {
                if ($exist) {
                    DB::table('desa_kades')->where('id', $request->id)->update($data_balai);
                } else {
                    $data_balai['id'] = $request->id;
                    DB::table('desa_kades')->insert($data_balai);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $th) {
                DB::rollback();
                $oke = false;
                dd($th);
            }

            if ($oke) {

                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Berhasil mengubah Informasi Balai Desa'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);

                return $responses;
            } else {

                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal mengubah Informasi Balai Desa'
                );
                return $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            }
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => $valid->errors()->first()
            );
            return $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
        }
    }
}
