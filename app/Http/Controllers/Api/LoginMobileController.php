<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\User\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class LoginMobileController extends Controller
{
    //

    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');

        $user = User::where('email', '=', $request->email)
            ->whereIn('idrole', ['1', '2', '3', '4', '5'])->get();

        if (count($user) > 0) {

            $checkPassword = password_verify($request->password, $user->first()->password);

            if ($checkPassword) {

                $userJoinsDetail = Users::join('users_detail as b', function ($joinDetailUser) {
                    $joinDetailUser->on('users.iduser', 'b.iduser');
                })
                    ->first();
                $dataUserOld = $user->first();

                $dataUserOld->token = $request->token_fcm;

                $dataUserOld->save();
                $tmpUserProfileArr = array(
                    'idUser'    => $userJoinsDetail->iduser,
                    'username'  => $userJoinsDetail->username,
                    'email'  => $userJoinsDetail->email,
                    'nama'  => $userJoinsDetail->nama,
                    'gender'  => $userJoinsDetail->gender,
                    'alamat'  => $userJoinsDetail->alamat,
                    'telepon'  => $userJoinsDetail->telepon,
                    'token'  => $userJoinsDetail->token,
                );



                $dataUserWithProfile = array(
                    'iduser'    => $dataUserOld->iduser,
                    'username'    => $dataUserOld->username,
                    'email'    => $dataUserOld->email,
                    'created_at'    => $dataUserOld->created_at,
                    'updated_at'    => $dataUserOld->updated_at,
                    'idrole'    => $dataUserOld->idrole,
                    'active'    => $dataUserOld->active,
                    'profile_photo_url'    => $dataUserOld->profile_photo_url,
                    'userProfile'    => $tmpUserProfileArr,
                );
                $responses = array(
                    'Status'    => true,
                    'Data'      => $dataUserWithProfile,
                    'Message'   => 'Login berhasil'
                );

                $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } else {
                $responses = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Kata sandi / password salah'
                );

                $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
                return $responses;
            }
        } else {
            $responses = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Username tidak terdaftar'
            );

            $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }

    public function changePassword(Request $request)
    {


        $user = User::where('iduser', '=', $request->idUser)->first();

        if (!empty($user)) {

            $checkPassword = password_verify($request->passwordOld, $user->password);

            if ($checkPassword) {

                $passwordNew = $request->passwordNew;
                $user->password =  Hash::make($passwordNew);
                $user->save();

                $responses = array(
                    'Status'    => true,
                    'Data'      => [],
                    'Message'   => 'Password Berhasil dirubah'
                );

                $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } else {
                $responses = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Kata sandi / password salah'
                );

                $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
                return $responses;
            }
        } else {
            $responses = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => 'Username tidak terdaftar'
            );

            $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
            return $responses;
        }
    }


    public function testPost(Request $request)
    {
        echo json_encode($request->all());
        echo 'tes';
    }
}
