<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Konten\Content;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class BeritaMobileController extends Controller
{
    //

    public function GetBeritas()
    {
        $beritas = Content::join('users as u', function ($userJoin) {
            $userJoin->on('content.idAuthor', 'u.idUser');
        })
            ->where('idKategori', 1)->orderBy('idcontent', 'desc')->get([
                'u.email', 'content.idContent', 'content.idAuthor', 'content.judul', 'content.poster', 'content.content', 'content.slug', 'content.created_at'
            ]);
        $dataBeritaArr = array();
        if (!empty($beritas)) {
            foreach ($beritas as $berita) {
                $tmpArr = array(
                    'id'    => $berita->idContent,
                    'email'    => $berita->email,
                    'judul'    => $berita->judul,
                    'poster'    => $berita->poster,
                    'content'   => strip_tags(substr_replace($berita->content, "....", 520)),
                    'slug'    => $berita->slug,
                    'fullImageUrl'    => asset('upload/content/' . $berita->slug . '/' . $berita->poster),
                    'created' => $berita->created_at != null ? date('d-m-Y', strtotime($berita->created_at)) : '',
                );
                $dataBeritaArr[] = $tmpArr;
            }
        }

        $responses = array(
            'Status'    => !empty($dataBeritaArr) ? true : false,
            'Data'      => $dataBeritaArr,
            'Message'   => 'Data Berita'
        );
        $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
        return $responses;
    }

    public function GetBerita(Request $request)
    {

        $berita = Content::join('users as u', function ($userJoin) {
            $userJoin->on('content.idAuthor', 'u.idUser');
        })
            ->where('content.idKategori', 1)
            ->where('content.idContent', $request->idContent)
            ->orderBy('idcontent', 'desc')
            ->first([
                'u.email', 'content.idContent', 'content.idAuthor', 'content.judul', 'content.poster', 'content.content', 'content.slug', 'content.created_at'
            ]);
        $dataBeritaArr = array();
        if (!empty($berita)) {
            $tmpArr = array(
                'id'    => $berita->idContent,
                'email'    => $berita->email,
                'judul'    => $berita->judul,
                'poster'    => $berita->poster,
                'poster'    => strip_tags($berita->content),
                'slug'    => $berita->slug,
                'fullImageUrl'    => asset('upload/content/' . $berita->slug . '/' . $berita->poster),
                'created' => $berita->created_at != null ? date('d-m-Y', strtotime($berita->created_at)) : '',
            );
            $dataBeritaArr = $tmpArr;
        }

        $responses = array(
            'Status'    => !empty($dataBeritaArr) ? true : false,
            'Data'      => $dataBeritaArr,
            'Message'   => 'Data Berita'
        );
        $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
        return $responses;
    }
}
