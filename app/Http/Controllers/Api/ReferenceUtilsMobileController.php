<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\MyMailResetPassword;
use App\Models\Refactored\Users\UsersDetail;
use App\Models\Refactored\Utils\UtilsDesa;
use App\Models\Refactored\Utils\UtilsKecamatan;
use App\Models\Refactored\Utils\UtilsKota;
use App\Models\Refactored\Utils\UtilsProvinsi;
use App\Models\User;
use App\Models\User\Users;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class ReferenceUtilsMobileController extends Controller
{
    //

    public function UtilProvices()
    {
        $provinces = UtilsProvinsi::where('active', 1)->get();

        $data_provinsi_arr = array();
        if (count($provinces) > 0) {
            foreach ($provinces as $province) {
                $tmp_arr = array(
                    'IdProvinsi'    => $province->id,
                    'NamaProvinsi'  => $province->name,
                );

                $data_provinsi_arr[] = $tmp_arr;
            }
            $responses_arr = array(
                'Status'   => true,
                'Data'      => $data_provinsi_arr,
                'Message'   => 'Data ditemukan'
            );
            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
        } else {
            $responses_arr = array(
                'Status'   => false,
                'Data'      => $data_provinsi_arr,
                'Message'   => 'Data tidak ada'
            );
            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
        }

        return $responses;
    }


    public function UtilCitys(Request $request)
    {


        $data_kota_arr = array();
        if ($request->idProvinsi == null) {
            $responses_arr = array(
                'Status'    => false,
                'Data'      => $data_kota_arr,
                'Message'   => 'Id Provinsi tidak boleh kosong'
            );

            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
        } else {
            $kotas = UtilsKota::where('active', 1)
                ->where('provinsiid', $request->idProvinsi)->get();

            if (count($kotas)) {
                foreach ($kotas as $kota) {
                    $tmp_arr = array(
                        'IdCity'    => $kota->id,
                        'NamaKota'  => $kota->name,
                    );
                    $data_kota_arr[] = $tmp_arr;
                }

                $responses_arr = array(
                    'Status'    => true,
                    'Data'      => $data_kota_arr,
                    'Message'   => 'Data ditemukan'
                );

                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
            } else {
                $responses_arr = array(
                    'Status'    => false,
                    'Data'      => $data_kota_arr,
                    'Message'   => 'Data tidak ditemukan'
                );

                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
            }
        }

        return $responses;
    }

    public function UtilsDistricts(Request $request)
    {
        $data_kecamatan_arr = array();
        if ($request->idKota == null) {
            $responses_arr = array(
                'Status'    => false,
                'Data'      => $data_kecamatan_arr,
                'Message'   => 'Id Kota tidak boleh kosong'
            );

            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
        } else {
            $objDistricts = UtilsKecamatan::where('active', 1)->where('kotaid', $request->idKota)->get();
            if (count($objDistricts) > 0) {
                foreach ($objDistricts as $objDistrict) {
                    $tmp_arr = array(
                        'IdKecamatan'    => $objDistrict->id,
                        'NamaKecamatan'  => $objDistrict->name,
                    );

                    $data_kecamatan_arr[] = $tmp_arr;
                }
                $responses_arr = array(
                    'Status'    => true,
                    'Data'  => $data_kecamatan_arr,
                    'Message'   => 'Data ditemukan'
                );

                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
            } else {
                $responses_arr = array(
                    'Status'    => false,
                    'Data'  => $data_kecamatan_arr,
                    'Message'   => 'Data Kecamatan tidak ditemukan'
                );

                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
            }
        }

        return $responses;
    }

    public function UtilVillages(Request $request)
    {

        $data_desa_arr = array();
        if ($request->idKecamatan == null) {
            $responses_arr = array(
                'Status'    => false,
                'Data'  => $data_desa_arr,
                'Message'   => 'Id Kecamatan tidak boleh kosong'
            );

            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
        } else {

            $villlages = UtilsDesa::where('active', 1)->where('kecamatanid', $request->idKecamatan)->get();

            if (count($villlages) > 0) {

                foreach ($villlages as $villlage) {
                    $tmp_arr = array(
                        'IdDesa'    => $villlage->id,
                        'NamaDesa'  => $villlage->name
                    );

                    $data_desa_arr[] = $tmp_arr;
                }

                $responses_arr = array(
                    'Status'    => true,
                    'Data'      => $data_desa_arr,
                    'Message'  => 'Data ditemukan'
                );

                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
            } else {
                $responses_arr = array(
                    'Status'    => false,
                    'Data'      => $data_desa_arr,
                    'Message'   => 'Data tidak ditemukan'
                );

                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
            }
        }

        return $responses;
    }

    public function UtilsJenisKelamins()
    {
        $dataJenisKelamin = array(
            [
                "id"    => "0",
                "item"  => 'Pilih',
            ],
            [
                "id"    => 'l',
                "item"  => 'Laki - laki',
            ],
            [
                "id"    => 'p',
                "item"  => 'Perempuan',
            ],
        );
        $responses_arr = array(
            'Status'    => true,
            'Data'      => $dataJenisKelamin,
            'Message'   => ''
        );
        return response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
    }

    public function UtilsYaTidak()
    {
        $datas = array(
            [
                "id"    => 0,
                "item"  => 'Tidak',
            ],
            [
                "id"    => 1,
                "item"  => 'Ya',
            ],

        );
        $responses_arr = array(
            'Status'    => true,
            'Data'      => $datas,
            'Message'   => ''
        );
        return response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
    }
    public function UtilsKondisis()
    {
        $datas = array(
            [
                "id"    => "0",
                "item"  => 'Rusak',
            ],
            [
                "id"    => 1,
                "item"  => 'Baik',
            ],

        );
        $responses_arr = array(
            'Status'    => true,
            'Data'      => $datas,
            'Message'   => ''
        );
        return response()->json($responses_arr, HttpFoundationResponse::HTTP_OK);
    }

    public function resetPassword(Request $request)
    {

        $checkUseEmail = Users::where('email', $request->email)->first();
        if (!empty($checkUseEmail)) {
            $raw_pass = strtolower($this->generatePassword(6));

            $userByid = Users::where('iduser', $checkUseEmail->iduser)->first();

            $userByid->password = Hash::make($raw_pass);
            $userByid->created_at = Carbon::now('Asia/Jakarta')->toDateTimeString();
            $userByid->updated_at = Carbon::now('Asia/Jakarta')->toDateTimeString();

            $userByid->save();

            $details = [
                'title' => 'Reset Password',
                'password_reset'  => $raw_pass
            ];
            Mail::to($request->email)->send(new MyMailResetPassword($details));

            if (Mail::failures()) {
                // return failed mails
                return new Error(Mail::failures());
            } else {
                $responses = array(
                    'Status'    => true,
                    'Data'      => [],
                    'Message'   => 'Permintaan reset password telah dikirim melalui email'
                );
                return $responses;
            }
        } else {
            $responses_arr = array(
                'Status'   => false,
                'Data'      => array(),
                'Message'   => 'Data ditemukan user email ' . $request->email . ' Tidak ditemukan'
            );
            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }

    public function updateProfileUser(Request $request)
    {

        $checkUsers = Users::where('iduser', $request->idUser)->first();

        if (!empty($checkUsers)) {
            DB::beginTransaction();

            try {
                //code...
                $checkUsers->username = $request->username;
                $checkUsers->email = $request->email;
                $checkUsers->updated_at = Carbon::now('Asia/Jakarta')->toDateTimeString();
                $checkUsers->save();

                $checkUserDetail = UsersDetail::where('iduser', $request->idUser)->first();
                if ($checkUserDetail == null) {
                    $objUserDetail = new UsersDetail();
                    $objUserDetail->iduser = $checkUsers->iduser;
                    $objUserDetail->nama = $request->name;
                    $objUserDetail->gender = $request->gender;
                    $objUserDetail->alamat = $request->address;
                    $objUserDetail->telepon = $request->phone;
                    $objUserDetail->save();
                } else {
                    $checkUserDetail->nama = $request->name;
                    $checkUserDetail->gender = $request->gender;
                    $checkUserDetail->alamat = $request->address;
                    $checkUserDetail->telepon = $request->phone;

                    $checkUserDetail->save();
                }


                DB::commit();
                $responses = array(
                    'Status'    => true,
                    'Data'      => null,
                    'Message'   => 'Data user profile success updated'
                );

                $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } catch (QueryException $ex) {
                //throw $th;
                $responses_arr = array(
                    'Status'   => false,
                    'Data'      => array(),
                    'Message'   => 'Databeses Error ' . $ex->getMessage()
                );
                $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_NOT_FOUND);
                return $responses;
            }
        } else {
            $responses_arr = array(
                'Status'   => false,
                'Data'      => array(),
                'Message'   => 'Data ditemukan id user ' . $request->idUser . ' Tidak ditemukan'
            );
            $responses = response()->json($responses_arr, HttpFoundationResponse::HTTP_NOT_FOUND);

            return $responses;
        }
    }

    private function generatePassword($length = 8)
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
