<?php

namespace App\Http\Controllers\Api;

use App\Exports\MonevExport;
use App\Exports\MonevSheetsExport;
use App\Http\Controllers\Controller;
use App\Models\Refactored\Monev\BobotKecukupans;
use App\Models\Refactored\Monev\BobotKetersediaans;
use App\Models\Refactored\Monev\BobotKondisi;
use App\Models\Refactored\Monev\BobotKondisis;
use App\Models\Refactored\Monev\DataMonevs;
use App\Models\Refactored\Monev\DetailMonevs;
use App\Models\Refactored\Monev\DetailPertanyaanMonevs;
use App\Models\Refactored\Monev\Indikators;
use App\Models\Refactored\Monev\Variabels;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Mockery\Undefined;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

use Validator;

class MonevMobileController extends Controller
{
    /**
     * response json
     */
    protected $resp = [
        'data' => null,
        'meta' => [
            'message' => 'Berhasil',
            'status_code' => 200
        ]
    ];

    /**
     * kategori pertanyaan
     */
    protected $kategori;

    /**
     *  indexVariabel
     *
     * @return view
     */
    public function indexVariabel()
    {
        $variabel = Variabel::orderBy('nama')->orderBy('jenis')->get();
        $bobotKecukupan = BobotKecukupan::first() !== null ? BobotKecukupan::first() : [
            'cukup' => 0,
            'lumayan_cukup' => 0,
            'tidak_cukup' => 0
        ];
        $bobotKondisi = BobotKondisi::first() !== null  ? BobotKondisi::first() : [
            'layak_atas' => 0,
            'layak_bawah' => 0,
            'cukup_layak_atas' => 0,
            'cukup_layak_bawah' => 0,
            'tidak_layak_atas' => 0,
            'tidak_layak_bawah' => 0
        ];
        $bobotKetersediaan = BobotKetersediaan::first() ? BobotKetersediaan::first() : [
            'ya'  => 0,
            'tidak'  => 0
        ];

        if (is_array($bobotKecukupan)) {
            $bobotKecukupan = json_decode(json_encode($bobotKecukupan));
        }

        if (is_array($bobotKondisi)) {
            $bobotKondisi = json_decode(json_encode($bobotKondisi));
        }

        if (is_array($bobotKetersediaan)) {
            $bobotKetersediaan = json_decode(json_encode($bobotKetersediaan));
        }

        return view('pages.master', compact('variabel', 'bobotKecukupan', 'bobotKondisi', 'bobotKetersediaan'));
    }

    /**
     * simpanVariabel
     *
     * @return view
     */
    public function simpanVariabel(Request $request)
    {
        $request->validate([
            'nama' => 'string|required|max:100',
            'jenis' => 'string|required|max:50',
            'presentase' => 'numeric|min:0|max:100|required'
        ]);

        $variabel = Variabel::create($request->all());

        if ($variabel) {
            return redirect()->back()->with('success', 'Berhasil menambah data variabel');
        }
        return redirect()->back()->withErrors(['Terjadi kesalahan pada server']);
    }

    /**
     * hapusVariabel
     *
     * @return view
     */
    public function hapusVariabel($id)
    {
        $variabel = Variabel::find($id);
        if ($variabel) {
            $variabel->delete();
            return redirect()->back()->with('success', 'Variabel berhasil dihapus');
        }
        return redirect()->back()->withErrors(['Variabel tidak ditemukan']);
    }

    /**
     * ubahVariabel
     *
     * @return view
     */
    public function ubahVariabel(Request $request)
    {
        $request->validate([
            'id' => 'numeric|required',
            'nama' => 'string|required|max:100',
            'jenis' => 'string|required|max:50',
            'presentase' => 'numeric|min:0|max:100|required'
        ]);

        $variabel = Variabel::find($request->id);
        if ($variabel) {
            $variabel->update($request->all());
            return redirect()->back()->with('success', 'Variabel berhasil diubah');
        }
        return redirect()->back()->withErrors(['Variabel tidak ditemukan']);
    }

    /**
     *  indexVariabel
     *
     * @return view
     */
    public function indexIndikator($variabel_id)
    {
        $variabel = Variabel::find($variabel_id);
        if ($variabel) {
            $indikator = $variabel->indikators()->orderBy('nama')->orderBy('type')->get();

            return view('pages.indikator', compact('indikator', 'variabel'));
        }

        return redirect()->back()->withErrors(['Variabel tidak ditemukan']);
    }

    /**
     * simpanIndikator
     *
     * @return view
     */
    public function simpanIndikator(Request $request)
    {
        $request->validate([
            'nama' => 'string|required|max:100',
            'variabel_id' => 'numeric|required',
            'type' => 'string|required|max:6'
        ]);

        $indikator = Indikator::create($request->all());

        if ($indikator) {
            return redirect()->back()->with('success', 'Berhasil menambah data indikator');
        }
        return redirect()->back()->withErrors(['Terjadi kesalahan pada server']);
    }

    /**
     * hapusIndikator
     *
     * @return view
     */
    public function hapusIndikator($id)
    {
        $indikator = Indikator::find($id);
        if ($indikator) {
            $indikator->delete();
            return redirect()->back()->with('success', 'Indikator berhasil dihapus');
        }
        return redirect()->back()->withErrors(['Indikator tidak ditemukan']);
    }

    /**
     * ubaIndikator
     *
     * @return view
     */
    public function ubahIndikator(Request $request)
    {
        $request->validate([
            'id' => 'numeric|required',
            'nama' => 'string|required|max:100',
            'variabel_id' => 'numeric|required',
            'type' => 'string|required|max:6'
        ]);

        $indikator = Indikator::find($request->id);
        if ($indikator) {
            $indikator->update($request->all());
            return redirect()->back()->with('success', 'Indikator berhasil diubah');
        }
        return redirect()->back()->withErrors(['Indikator tidak ditemukan']);
    }

    /**
     *  indexVariabel
     *
     * @return view
     */
    public function indexPertanyaan($variabel_id, $indikator_id)
    {
        $variabel = Variabel::find($variabel_id);

        if (!$variabel) {
            return redirect()->back()->withErrors(['Variabel tidak ditemukan']);
        }

        $indikator = Indikator::find($indikator_id);
        if ($indikator) {
            $pertanyaan = $indikator->pertanyaans()->orderBy('pertanyaan')->get();

            return view('pages.pertanyaan', compact('pertanyaan', 'indikator', 'variabel'));
        }

        return redirect()->back()->withErrors(['Indikator tidak ditemukan']);
    }

    /**
     * simpanIndikator
     *
     * @return view
     */
    public function simpanPertanyaan(Request $request)
    {
        $request->validate([
            'pertanyaan' => 'string|required|max:255',
            'indikator_id' => 'numeric|required',
            // 'lokpri_batas_darat' => 'boolean|required',
            // 'lokpri_batas_laut' => 'boolean|required',
            // 'lokpri_pksn_darat' => 'boolean|required',
            // 'lokpri_pksn_laut' => 'boolean|required',
            // 'lokpri_ppkt' => 'boolean|required'
        ]);

        $input = $request->all();

        if (array_key_exists('lokpri_batas_darat', $input)) {
            $input['lokpri_batas_darat'] = true;
        }

        if (array_key_exists('lokpri_batas_laut', $input)) {
            $input['lokpri_batas_laut'] = true;
        }

        if (array_key_exists('lokpri_pksn_darat', $input)) {
            $input['lokpri_pksn_darat'] = true;
        }

        if (array_key_exists('lokpri_pksn_laut', $input)) {
            $input['lokpri_pksn_laut'] = true;
        }

        if (array_key_exists('lokpri_ppkt', $input)) {
            $input['lokpri_ppkt'] = true;
        }

        $pertanyaan = Pertanyaan::create($input);


        if ($pertanyaan) {
            return redirect()->back()->with('success', 'Berhasil menambah data indikato$indikator');
        }
        return redirect()->back()->withErrors(['Terjadi kesalahan pada server']);
    }

    /**
     * hapusIndikator
     *
     * @return view
     */
    public function hapusPertanyaan($id)
    {
        $pertanyaan = Pertanyaan::find($id);
        if ($pertanyaan) {
            $pertanyaan->delete();
            return redirect()->back()->with('success', 'Pertanyaan berhasil dihapus');
        }
        return redirect()->back()->withErrors(['Pertanyaan tidak ditemukan']);
    }

    /**
     * ubaIndikator
     *
     * @return view
     */
    public function ubahPertanyaan(Request $request)
    {
        $request->validate([
            'pertanyaan' => 'string|required|max:255',
            'indikator_id' => 'numeric|required',

        ]);

        $input = $request->all();

        if (array_key_exists('lokpri_batas_darat', $input)) {
            $input['lokpri_batas_darat'] = true;
        } else {
            $input['lokpri_batas_darat'] = false;
        }

        if (array_key_exists('lokpri_batas_laut', $input)) {
            $input['lokpri_batas_laut'] = true;
        } else {
            $input['lokpri_batas_laut'] = false;
        }

        if (array_key_exists('lokpri_pksn_darat', $input)) {
            $input['lokpri_pksn_darat'] = true;
        } else {
            $input['lokpri_pksn_darat'] = false;
        }

        if (array_key_exists('lokpri_pksn_laut', $input)) {
            $input['lokpri_pksn_laut'] = true;
        } else {
            $input['lokpri_pksn_laut'] = false;
        }

        if (array_key_exists('lokpri_ppkt', $input)) {
            $input['lokpri_ppkt'] = true;
        } else {
            $input['lokpri_ppkt'] = false;
        }

        $pertanyaan = Pertanyaan::find($request->id);
        if ($pertanyaan) {
            $pertanyaan->update($input);
            return redirect()->back()->with('success', 'Pertanyaan berhasil diubah');
        }
        return redirect()->back()->withErrors(['Pertanyaan tidak ditemukan']);
    }

    /**
     * simpanBobotNilai
     *
     * @return view
     */
    public function simpanBobotNilai(Request $request)
    {
        $request->validate([
            'ketersediaan_tidak' => 'numeric|min:0|max:100',
            'ketersediaan_ya' => 'numeric|min:0|max:100',
            'kecukupan_tidak' => 'numeric|min:0|max:100',
            'kecukupan_lumayan' => 'numeric|min:0|max:100',
            'kecukupan_cukup' => 'numeric|min:0|max:100',
            'kondisi_tidak_layak_bawah' => 'numeric|min:0|max:100',
            'kondisi_tidak_layak_atas' => 'numeric|min:0|max:100',
            'kondisi_cukup_layak_bawah' => 'numeric|min:0|max:100',
            'kondisi_cukup_layak_atas' => 'numeric|min:0|max:100',
            'kondisi_cukup_bawah' => 'numeric|min:0|max:100',
            'kondisi_cukup_atas' => 'numeric|min:0|max:100',
        ]);

        $kecukupan = [
            'cukup' => $request->kecukupan_cukup,
            'lumayan_cukup' => $request->kecukupan_lumayan,
            'tidak_cukup' => $request->kecukupan_tidak
        ];

        $kondisi = [
            'layak_atas' => $request->kondisi_cukup_atas,
            'layak_bawah' => $request->kondisi_cukup_bawah,
            'cukup_layak_atas' => $request->kondisi_cukup_layak_atas,
            'cukup_layak_bawah' => $request->kondisi_cukup_layak_bawah,
            'tidak_layak_atas' => $request->kondisi_tidak_layak_atas,
            'tidak_layak_bawah' => $request->kondisi_tidak_layak_bawah
        ];

        $ketersediaan = [
            'ya'  => $request->ketersediaan_ya,
            'tidak'  => $request->ketersediaan_tidak
        ];

        $bobotKecukupan = BobotKecukupan::first();
        if ($bobotKecukupan == null) {
            $bobotKecukupan = BobotKecukupan::create($kecukupan);
        } else {
            $bobotKecukupan->update($kecukupan);
        }


        $bobotKetersediaan = BobotKetersediaan::first();
        if ($bobotKetersediaan == null) {
            $bobotKetersediaan = BobotKetersediaan::create($ketersediaan);
        } else {
            $bobotKetersediaan->update($ketersediaan);
        }

        $bobotKondisi = BobotKondisi::first();
        if ($bobotKondisi == null) {
            $bobotKondisi = BobotKondisi::create($kondisi);
        } else {
            $bobotKondisi->update($kondisi);
        }


        if ($bobotKecukupan && $bobotKetersediaan && $bobotKondisi) {
            return redirect('/master/variabel')->with('success', 'Berhasil memperbaharui bobot nilai');
        }
        return redirect()->back()->withErrors(['Terjadi kesalahan pada server']);
    }

    /**
     * simpanTemperory
     *
     * @return json
     */
    public function simpanTemperory(Request $request)
    {

        $validator = Validator::make($request->all(), [
            /* 'nama' => 'required|string|max:20',
            'jabatan' => 'required|string|max:50',
            'alamat_kantor' => 'required|string|max:255',
            'email' => 'required|email|max:100',
            'no_hp' => 'required|string|max:13',
            'provinsi' => 'required|string|max:50',
            'kabupaten' => 'required|string|max:50',
            'kecamatan' => 'required|string|max:50',
            'kategori' => 'required|string', */
            'IdJenisLokpri' => 'required|int',
            'IdPegawai' => 'required|int'

        ]);


        if ($validator->fails()) {
            $msgs = $validator->errors()->getMessages();
            $msg = '';
            $temp = 0;
            foreach ($msgs as $m) {
                $msg .= $m[0];
                if ($temp != count($msgs) - 1) {
                    $msg .= ' ';
                }
            }
            $msg = '';

            if (!$request->has('IdJenisLokpri')) {
                $msg .= ' IdJenis Lokpri tidak boleh kosong.';
            }

            if (!$request->has('IdPegawai')) {
                $msg .= ' IdPegawa tidak boleh kosong.';
            }


            return response()->json([
                'data' => null,
                'meta' => [
                    'message' => $msg,
                    'status_code' => 406,
                ]
            ], 200);
        }


        $input = $request->all();
        $input['kecamatanid']   = $request->IdKecamatan;
        $input['id_user']       = $request->IdPegawai;
        $input['nama']       = $request->Nama;
        $input['jabatan']       = $request->Jabatan;
        $input['alamat_kantor']       = $request->AlamatKantor;
        $input['email']       = $request->Email;
        $input['no_hp']       = $request->Nohp;

        if ($input['IdJenisLokpri'] == 1) {
            $input['lokpri_batas_darat'] = true;
            $input['lokpri_batas_laut'] = false;
            $input['lokpri_pksn_darat'] = false;
            $input['lokpri_pksn_laut'] = false;
            $input['lokpri_ppkt'] = false;
        } else if ($input['kategori'] == 2) {
            $input['lokpri_batas_darat'] = false;
            $input['lokpri_batas_laut'] = true;
            $input['lokpri_pksn_darat'] = false;
            $input['lokpri_pksn_laut'] = false;
            $input['lokpri_ppkt'] = false;
        } else if ($input['kategori'] == 3) {
            $input['lokpri_batas_darat'] = false;
            $input['lokpri_batas_laut'] = false;
            $input['lokpri_pksn_darat'] = true;
            $input['lokpri_pksn_laut'] = false;
            $input['lokpri_ppkt'] = false;
        } else if ($input['kategori'] == 4) {
            $input['lokpri_batas_darat'] = false;
            $input['lokpri_batas_laut'] = false;
            $input['lokpri_pksn_darat'] = false;
            $input['lokpri_pksn_laut'] = true;
            $input['lokpri_ppkt'] = false;
        } else if ($input['kategori'] == 5) {
            $input['lokpri_batas_darat'] = false;
            $input['lokpri_batas_laut'] = false;
            $input['lokpri_pksn_darat'] = false;
            $input['lokpri_pksn_laut'] = false;
            $input['lokpri_ppkt'] = true;
        }

        unset($input['kategori']);



        $dataMonev = DataMonevs::where('kecamatanid', $request->IdKecamatan)->whereYear('updated_at', date('Y'))->first();

        $this->kategori = $request->kategori;

        if ($dataMonev !== null) {

            $lokpriBatasDarat = $dataMonev->lokpri_batas_darat == 1 ? true : false;
            $lokpriBatasLaut = $dataMonev->lokpri_batas_laut == 1 ? true : false;
            $lokpriPKSNDarat = $dataMonev->lokpri_pksn_darat == 1 ? true : false;
            $lokpriPKSNLaut = $dataMonev->lokpri_pksn_laut == 1 ? true : false;
            $lokpriPPKT = $dataMonev->lokpri_ppkt == 1 ? true : false;

            if ($dataMonev->rata_rata_pap !== null || $dataMonev->rata_rata_ciq !== null) {
                unset($input['lokpri_batas_darat']);
                unset($input['lokpri_batas_laut']);
                unset($input['lokpri_pksn_darat']);
                unset($input['lokpri_pksn_darat']);
                unset($input['lokpri_pksn_laut']);
                unset($input['lokpri_ppkt']);
                $kategori = '';
                if ($dataMonev->lokpri_batas_darat == 1) {
                    $kategori = 'lokpri_batas_darat';
                } else if ($dataMonev->lokpri_batas_laut == 1) {
                    $kategori = 'lokpri_batas_laut';
                } else if ($dataMonev->lokpri_pksn_darat == 1) {
                    $kategori = 'lokpri_pksn_darat';
                } else if ($dataMonev->lokpri_pksn_laut == 1) {
                    $kategori = 'lokpri_pksn_laut';
                } else if ($dataMonev->lokpri_ppkt == 1) {
                    $kategori = 'lokpri_ppkt';
                }

                $this->resp['meta']['kategori'] = $kategori;
                $this->kategori = $kategori;
            } else {
                $dataYgAktif = $this->checkDataAktif($dataMonev);
                $inputYgAktif = $this->checkInputAktif($input);

                if ($dataYgAktif !== $inputYgAktif) {
                    $this->hapusForm($dataMonev);
                    $this->buatForm($dataMonev, $inputYgAktif);
                }
            }
            // dd($dataMonev);
            $dataMonev->update($input);
            $form = $this->getForm($dataMonev);

            $this->resp['data'] = $form;
            $this->resp['meta']['message'] = 'Anda sudah menginputkan data sebelumnya';
            $this->resp['meta']['status_code'] = 200;
            return response()->json($this->resp, 200);
        }


        $monev = DataMonevs::create($input);

        if ($monev) {
            $kategori = $this->checkDataAktif($monev);

            $this->buatForm($monev, $kategori);

            $this->resp['data'] = $this->getForm($monev, $kategori);

            return response()->json($this->resp, 200);
        }

        $this->resp['meta']['message'] = 'Internal Server Error';
        $this->resp['meta']['status_code'] = 500;
        return response()->json($this->resp, 500);
    }

    /**
     * checkDataAktif
     *
     * @return  string
     */
    public function checkDataAktif($dataMonev)
    {
        $dataYgAktif = '';

        if ($dataMonev->lokpri_batas_darat == 1) {
            $dataYgAktif = 'lokpri_batas_darat';
        } else if ($dataMonev->lokpri_batas_laut == 1) {
            $dataYgAktif = 'lokpri_batas_laut';
        } else if ($dataMonev->lokpri_pksn_darat == 1) {
            $dataYgAktif = 'lokpri_pksn_darat';
        } else if ($dataMonev->lokpri_pksn_laut == 1) {
            $dataYgAktif = 'lokpri_pksn_laut';
        } else if ($dataMonev->lokpri_ppkt == 1) {
            $dataYgAktif = 'lokpri_ppkt';
        }

        return $dataYgAktif;
    }

    /**
     * checkInputAktif
     *
     * @return string
     */
    public function checkInputAktif($input)
    {
        $inputYgAktif = '';

        if ($input['lokpri_batas_darat'] == 1) {
            $inputYgAktif = 'lokpri_batas_darat';
        } else if ($input['lokpri_batas_laut'] == 1) {
            $inputYgAktif = 'lokpri_batas_laut';
        } else if ($input['lokpri_pksn_darat'] == 1) {
            $inputYgAktif = 'lokpri_pksn_darat';
        } else if ($input['lokpri_pksn_laut'] == 1) {
            $inputYgAktif = 'lokpri_pksn_laut';
        } else if ($input['lokpri_ppkt'] == 1) {
            $inputYgAktif = 'lokpri_ppkt';
        }

        return $inputYgAktif;
    }

    /**
     * buatForm
     *
     * @return void
     */
    public function buatForm($monev, $kategori)
    {
        $variabels = Variabels::orderBy('nama')->orderBy('jenis')->get();

        foreach ($variabels as $variabel) {
            $var = [
                'variabel_id' => $variabel->id,
                'monev_id' => $monev->id
            ];
            $detailMonev = DetailMonevs::create($var);
            $indikators = $variabel->indikators()->orderBy('type')->get();
            foreach ($indikators as $indikator) {
                $this->simpanDetailPertanyaanTemperory($indikator, $detailMonev->id, $kategori);
            }
        }
    }

    /**
     * hapusForm
     *
     * @return void
     */
    public function hapusForm($monev)
    {
        $detailMonev = DetailMonevs::where('monev_id', $monev->id)->delete();
    }

    /**
     * simpanDetailPertanyaan
     *
     * Model $indikator
     *
     * @return void
     */
    public function simpanDetailPertanyaanTemperory($indikator, $detailMonevId, $kategori)
    {
        $pertanyaans = $indikator->pertanyaans()->where($kategori, 1)->orderBy('pertanyaan')->get();

        foreach ($pertanyaans as $pertanyaan) {
            DetailPertanyaanMonevs::create([
                'detail_monev_id' => $detailMonevId,
                'pertanyaan_id' => $pertanyaan->id,
            ]);
        }
    }

    /**
     * getForm
     *
     * model $dataMonev
     *
     * @return array
     */
    public function getForm($dataMonev)
    {
        $form = [
            'idMonev' => $dataMonev->id,
            'form' => []
        ];

        $detailMonev = DetailMonevs::where('monev_id', $dataMonev->id)->get();
        foreach ($detailMonev as $dm) {
            $variabel = $dm->variabel()->first();

            $var = [
                'variabelId' => $variabel->id,
                'variabel' => $variabel->nama,
                'jenis' => $variabel->jenis,
                'indikator' => $this->getIndikatorForm($dm, $variabel)
            ];

            array_push($form['form'], $var);
        }

        return $form;
    }

    /**
     * getIndikatorForm
     *
     * Model $detailMonev
     * Model $variabel
     *
     * @return array
     */
    public function getIndikatorForm($detailMonev, $variabel)
    {
        $indikator = [];
        $indikators = $variabel->indikators()->orderBy('type')->get();
        foreach ($indikators as $idktr) {
            $idn = [
                'indikatorId' => $idktr->id,
                'indikatorEvaluasi' => $idktr->nama,
                'type' => $idktr->type,
                'pertanyaan' => $this->getPertanyaanForm($detailMonev, $idktr)
            ];

            array_push($indikator, $idn);
        }

        return $indikator;
    }

    /**
     * getPertanyaanForm
     *
     * Model $detailMonev
     * Model $indikator
     *
     * @return array
     */
    public function getPertanyaanForm($detailMonev, $indikator)
    {
        $pertanyaan = [];
        $dataMonev = $detailMonev->dataMonev()->first();
        $pertanyaans = $detailMonev->detailPertanyaanMonev()->get();

        foreach ($pertanyaans as $pert) {
            $p = $pert->pertanyaan()->where('indikator_id', $indikator->id)->where($this->kategori, 1)->first();
            if ($p !== null) {
                $dataPert = [
                    'pertanyaanId' => $pert->id,
                    'pertanyaan' => $p->pertanyaan,
                    'ketersediaan' => $pert->ketersediaan,
                    'kecukupan' => $pert->kecukupan,
                    'kondisi' => $pert->kondisi,
                    'kebutuhan' => $pert->kebutuhan,
                    'keterisian' => $pert->keterisian,
                    'bobotNilaiPendidikan' => $pert->bobot_nilai_pendidikan,
                    'banyakPendidikan' => $pert->banyak_pendidikan
                ];
                array_push($pertanyaan, $dataPert);
            }
        }

        // dd($pertanyaans);
        return $pertanyaan;
    }

    /**
     * getBobotNilai
     *
     * @return json
     */
    public function getBobotNilai()
    {

        $bobotKecukupan = BobotKecukupan::first() !== null ? BobotKecukupan::first() : [
            'cukup' => 0,
            'lumayan_cukup' => 0,
            'tidak_cukup' => 0
        ];
        $bobotKondisi = BobotKondisi::first() !== null  ? BobotKondisi::first() : [
            'layak_atas' => 0,
            'layak_bawah' => 0,
            'cukup_layak_atas' => 0,
            'cukup_layak_bawah' => 0,
            'tidak_layak_atas' => 0,
            'tidak_layak_bawah' => 0
        ];
        $bobotKetersediaan = BobotKetersediaan::first() ? BobotKetersediaan::first() : [
            'ya'  => 0,
            'tidak'  => 0
        ];

        if (is_array($bobotKecukupan)) {
            $bobotKecukupan = json_decode(json_encode($bobotKecukupan));
        }

        if (is_array($bobotKondisi)) {
            $bobotKondisi = json_decode(json_encode($bobotKondisi));
        }

        if (is_array($bobotKetersediaan)) {
            $bobotKetersediaan = json_decode(json_encode($bobotKetersediaan));
        }

        $this->resp['data'] = [
            'bobot_kecukupan' => $bobotKecukupan,
            'bobot_kondisi' => $bobotKondisi,
            'bobot_Ketersediaan' => $bobotKetersediaan
        ];

        return response()->json($this->resp, 200);
    }


    /**
     * simpanTemp
     *
     * @return json
     */
    public function saveTemp(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'idMonev' => 'required|numeric',
            'form' => 'required|array',
        ]);

        if ($validator->fails()) {
            $msgs = $validator->errors()->getMessages();
            $msg = '';
            $temp = 0;
            foreach ($msgs as $m) {
                $msg .= $m[0];
                if ($temp != count($msgs) - 1) {
                    $msg .= ' ';
                }
            }

            return response()->json([
                'data' => null,
                'meta' => [
                    'message' => $msg,
                    'status_code' => 406,
                ]
            ], 406);
        }

        foreach ($request->form as $form) {

            foreach ($form['indikator'] as $indikator) {

                foreach ($indikator['pertanyaan'] as $pertanyaan) {
                    //echo $pertanyaan['pertanyaanId'];
                    $jawaban = DetailPertanyaanMonevs::find($pertanyaan['pertanyaanId']);
                    //  echo json_encode($jawaban);

                    if ($jawaban) {
                        unset($pertanyaan['pertanyaanId']);

                        try {
                            //code...
                            $jawaban->ketersediaan = $pertanyaan['ketersediaan'];
                            $jawaban->bobot_ketersediaan = null;
                            $jawaban->kecukupan = $pertanyaan['kecukupan'];
                            $jawaban->bobot_kecukupan = null;
                            $jawaban->kondisi = $pertanyaan['kondisi'];
                            $jawaban->bobot_kondisi = null;
                            $jawaban->kebutuhan = $pertanyaan['kebutuhan'];
                            $jawaban->keterisian = $pertanyaan['keterisian'];
                            $jawaban->bobot_nilai_pendidikan = $pertanyaan['bobotNilaiPendidikan'];
                            $jawaban->banyak_pendidikan = $pertanyaan['banyakPendidikan'];


                            $jawaban->save();
                        } catch (QueryException $ex) {
                            echo $ex;
                            //throw $th;
                            return $ex;
                        }
                    }
                }
            }
        }

        return response()->json($this->resp, 200);
    }

    /**
     * simpanHasikMonev
     */
    public function simpanHasilMonev($monev_id)
    {
        $monev = DataMonev::find($monev_id);

        if ($monev) {
            $bobotKetersediaan = BobotKetersediaan::first();
            $bobotKondisi = BobotKondisi::first();
            $bobotKecukupan = BobotKecukupan::first();
            $detailMonev = $monev->detailMonev()->get();

            $totalRataRata = [];
            foreach ($detailMonev as $dm) {
                $arrayIndikator = [];
                $arrayKetersediaan = [];
                $arrayKondisi = [];
                $arrayKecukupan = [];
                $arrayKebutuhan = [];
                $arrayPendidikan = [];

                $detailPertanyaan = $dm->detailPertanyaanMonev()->get();

                foreach ($detailPertanyaan as $pertanyaan) {

                    //ketersediaan
                    if ($pertanyaan->ketersediaan == 'Y') {
                        $pertanyaan->bobot_ketersediaan = $bobotKetersediaan->ya;
                    } else if ($pertanyaan->ketersediaan == 'T') {
                        $pertanyaan->bobot_ketersediaan = $bobotKetersediaan->tidak;
                    }

                    //kecukupan
                    if ($pertanyaan->kecukupan == 'Y') {
                        $pertanyaan->bobot_kecukupan = $bobotKecukupan->cukup;
                    } else if ($pertanyaan->kecukupan == 'T') {
                        $pertanyaan->bobot_kecukupan = $bobotKecukupan->tidak_cukup;
                    } else if ($pertanyaan->kecukupan == 'L') {
                        $pertanyaan->bobot_kecukupan = $bobotKecukupan->lumayan_cukup;
                    }

                    //kondisi
                    $pertanyaan->bobot_kondisi = $pertanyaan->kondisi;

                    //kebutuhan

                    $pertanyaan->bobot_keb_ket = $pertanyaan->kebutuhan == 0 ? null : (100 / $pertanyaan->kebutuhan) *  $pertanyaan->keterisian;

                    //pendidikan
                    $pertanyaan_ori = $pertanyaan->pertanyaan()->first();
                    $pertanyaan->bobot_nilai_pendidikan = $pertanyaan_ori->bobot_default;
                    $pertanyaan->jum_bobot_nilai_pendidikan = $pertanyaan->banyak_pendidikan * ($pertanyaan_ori->bobot_default == null ? 0 : $pertanyaan_ori->bobot_default);

                    $pertanyaan->save();

                    $indikator = $pertanyaan->pertanyaan()->first()->indikator()->first();

                    if ($indikator->type == 'Type 1') {
                        if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                            $arrayIndikator[$indikator->nama]['type'] = 'Type 1';
                            $arrayIndikator[$indikator->nama]['nilai'] = [
                                'ketersediaan' => [$pertanyaan->bobot_ketersediaan],
                                'kecukupan' => [$pertanyaan->bobot_kecukupan],
                                'kondisi' => [$pertanyaan->bobot_kondisi]
                            ];
                        } else {
                            array_push($arrayIndikator[$indikator->nama]['nilai']['ketersediaan'], $pertanyaan->bobot_ketersediaan);
                            array_push($arrayIndikator[$indikator->nama]['nilai']['kecukupan'],  $pertanyaan->bobot_kecukupan);
                            array_push($arrayIndikator[$indikator->nama]['nilai']['kondisi'], $pertanyaan->bobot_kondisi);
                        }
                    } else if ($indikator->type == 'Type 2') {
                        if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                            $arrayIndikator[$indikator->nama]['type'] = 'Type 2';
                            $arrayIndikator[$indikator->nama]['nilai']['keb_ket'] = [$pertanyaan->bobot_keb_ket];
                        } else {
                            array_push($arrayIndikator[$indikator->nama]['nilai']['keb_ket'], $pertanyaan->bobot_keb_ket);
                        }
                    } else if ($indikator->type == 'Type 3') {
                        if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                            $arrayIndikator[$indikator->nama]['type'] = 'Type 3';
                            $arrayIndikator[$indikator->nama]['nilai'] = [
                                [
                                    'jum_bobot' => $pertanyaan->jum_bobot_nilai_pendidikan,
                                    'banyak' => $pertanyaan->banyak_pendidikan,
                                ]
                            ];
                        } else {
                            array_push($arrayIndikator[$indikator->nama]['nilai'], [
                                'jum_bobot' => $pertanyaan->jum_bobot_nilai_pendidikan,
                                'banyak' => $pertanyaan->banyak_pendidikan
                            ]);
                        }
                    } else if ($indikator->type == 'Type 4') {
                        if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                            $arrayIndikator[$indikator->nama]['type'] = 'Type 4';
                            $arrayIndikator[$indikator->nama]['nilai'] = [
                                'ketersediaan' => [$pertanyaan->bobot_ketersediaan]
                            ];
                        } else {
                            array_push($arrayIndikator[$indikator->nama]['nilai']['ketersediaan'], $pertanyaan->bobot_ketersediaan);
                        }
                    }
                }

                $totalPerindikator = [];
                foreach ($arrayIndikator as $key => $avgPerIndikator) {
                    $avg1 = [];
                    if ($avgPerIndikator['type'] == 'Type 3') {
                        $a = [];
                        $b = [];
                        foreach ($avgPerIndikator['nilai'] as $avg) {
                            array_push($a, $avg['banyak']);
                            array_push($b, $avg['jum_bobot']);
                        }
                        $jum_bobot = array_sum($b);
                        $banyak = array_sum($a);
                        $totalPerindikator[$key] = $banyak == 0 ? 0 : $jum_bobot / $banyak;
                    } else {
                        foreach ($avgPerIndikator['nilai'] as $avg) {
                            $a = array_sum($avg) / count($avg);
                            array_push($avg1, $a);
                        }
                        $perIndikator = array_sum($avg1) / count($avg1);
                        $totalPerindikator[$key] = $perIndikator;
                    }
                }


                $presentase = $dm->variabel()->first()->presentase;
                $dm->rata_rata = count($totalPerindikator) !== 0 ? (array_sum($totalPerindikator) / count($totalPerindikator)) * ($presentase / 100) : 0;
                $dm->save();

                $jenis = $dm->variabel()->first()->jenis;
                if (array_key_exists($jenis, $totalRataRata)) {
                    array_push($totalRataRata[$jenis], $dm->rata_rata);
                } else {
                    $totalRataRata[$jenis] =  [$dm->rata_rata];
                }
            }

            if (array_key_exists('Membantu Pengawasan CIQ', $totalRataRata)) {
                $monev->rata_rata_ciq = array_sum($totalRataRata['Membantu Pengawasan CIQ']);
            }

            if (array_key_exists('Pelayanan Administrasi Pemerintahan', $totalRataRata)) {
                $monev->rata_rata_pap = array_sum($totalRataRata['Pelayanan Administrasi Pemerintahan']);
            }

            $monev->save();
            return response()->json($this->resp);
        }
    }

    /**
     * report
     *
     * @return view
     */
    public function GetMonevs(Request $request)
    {
        $query = DB::table('data_monevs as a')
            ->join('utils_kecamatan as b', 'a.kecamatanid', '=', 'b.id')
            ->join('utils_kota as c', 'b.kotaid', 'c.id')
            ->join('utils_provinsi as d', 'c.provinsiId', 'd.id')
            ->groupBy('a.kecamatanId');

        if ($tahun = $request->input('tahun')) {


            $query->where(DB::raw('YEAR(a.updated_at)'), '=', $tahun);
        }

        if ($sort = $request->input('sort')) {
            $query->orderBy('a.id', $sort);
        }


        $perPage = 0;
        $page = $request->input('perpage');

        if ($page == '' || $page == 0) {

            $perPage = 10;
        } else {
            $perPage = $page;
        }

        $page = $request->input('page', 1);
        $total = $query->count();
        $result =  $query->offset(($page - 1) * $perPage)->limit($perPage)
            ->get(['a.*', 'b.name as name_kecamatan', 'c.id as idCity', 'c.name as cityName', 'd.name as nameProv']);



        if ($result->count() > 0) {
            $monevArr = array();
            foreach ($result as $dm) {
                $tmpArr = array(
                    'id'    => $dm->id,
                    'distictId'    => $dm->kecamatanid,
                    'districtName'    => $dm->name_kecamatan,
                    'citiId'    => $dm->idCity,
                    'nama'      => $dm->nama,
                    'officeAddress'      => $dm->alamat_kantor,
                    'citiName'    => $dm->cityName,
                    'provName'    => $dm->nameProv,
                    'rataRataCiq'    => $dm->rata_rata_ciq,
                    'rataRataPap'    => $dm->rata_rata_pap,
                );
                array_push($monevArr, $tmpArr);
            }
            $responses = array(
                'Status'    => true,
                'Data'      => $monevArr,
                'total' => $total,
                'page' => $page,
                'last_page' => ceil($total / $perPage),
                'Message'   => 'Data Monev'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $responses = array(
                'Status'    => false,
                'Data'      => array(),
                'total' => 0,
                'page' => 0,
                'last_page' => 0,
                'Message'   => 'Not Found'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }

    /**
     * downloadSummaryReport
     *
     * @return json
     */

    public function summaryReportMonev(Request $request)
    {
        $tahun = date('Y');
        if ($request->has('tahun')) {
            $tahun = $request->tahun;
        }
        $dataMonev = DataMonevs::whereYear('updated_at', $tahun)
            ->orderBy('rata_rata_pap')
            ->get();

        return Excel::download(new MonevExport($dataMonev, $tahun), 'Laporan Hasil Monev Tahun' . $tahun . '.xlsx');
    }

    /**
     * detailReport
     *
     * @return json
     */
    public function detailReportById($monev_id, Request $request)
    {
        $paramMonevReplcace = str_replace(".xlsx", "", $monev_id);

        $monev = DataMonevs::find($paramMonevReplcace);
        $result = [
            'total_rata_rata' => 0,
            'detail_monev' => []
        ];
        if ($monev) {
            $bobotKetersediaan = BobotKetersediaans::first();
            $bobotKondisi = BobotKondisis::first();
            $bobotKecukupan = BobotKecukupans::first();
            $detailMonev = $monev->detail()->get();

            foreach ($detailMonev as $dm) {
                $detailPertanyaan = $dm->detailPertanyaanMonev()->get();
                $dataDetailIndikator = [];
                $inIndikator = [];
                foreach ($detailPertanyaan as $dp) {
                    $dp->pertanyaan = $dp->pertanyaan()->first()->pertanyaan;
                    $indikator = $dp->pertanyaan()->first()->indikator()->first();
                    if ($this->in_array_r($indikator->nama, $dataDetailIndikator)) {
                        $k = array_search($indikator->nama, $inIndikator);
                        // dd($dataDetailIndikator);
                        array_push($dataDetailIndikator[$k]['pertanyaan'], $dp);
                    } else {
                        array_push($inIndikator, $indikator->nama);
                        array_push($dataDetailIndikator, [
                            'indikator' => $indikator->nama,
                            // 'total_rata_rata' => $this->getRataRataPerIndikator($detailPertanyaan, $indikator),
                            'type' => $indikator->type,
                            'pertanyaan' => [$dp]
                        ]);
                    }
                }
                $variabel = $dm->variabel()->first();
                array_push($result['detail_monev'], [
                    'variabel' =>  $variabel,
                    'rata_rata' => $dm->rata_rata,
                    'indikator' => $dataDetailIndikator
                ]);
            }

            $monev->detail_monev = $result['detail_monev'];
            $monev = json_decode(json_encode($monev));
            //return Excel::download(new MonevSheetsExport($monev), 'Bukti Laporan Monev - ' . $monev->nama . '.xlsx');
            return Excel::download(new MonevSheetsExport($monev), 'Bukti Laporan Monev - ' . $monev_id);
        }
    }

    /**
     * in_array_r
     *
     * @return bool
     */
    public function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }


    /**
     * hapusVariabel
     *
     * @return view
     */
    public function hapusDataMonev($id)
    {
        $monev = DataMonev::find($id);
        if ($monev) {
            $monev->delete();
            return redirect()->back()->with('success', 'Data Monev berhasil dihapus');
        }
        return redirect()->back()->withErrors(['Data Monev tidak ditemukan']);
    }

    /**
     * getTotalCIQ
     *
     * @return view
     */
    public function getTotalCIQ($monev)
    {
        $total = DB::table('detail_monevs')
            ->join('variabels', 'detail_monevs.variabel_id', 'variabels.id')
            ->where('variabels.jenis', 'Membantu Pengawasan CIQ')
            ->where('detail_monevs.monev_id', $monev->id)
            ->select(DB::raw('AVG(detail_monevs.rata_rata) AS total'))->first();

        return $total->total;
    }

    /**
     * getTotalPAP
     *
     * @return view
     */
    public function getTotalPAP($monev)
    {
        $total = DB::table('detail_monevs')
            ->join('variabels', 'detail_monevs.variabel_id', 'variabels.id')
            ->where('variabels.jenis', 'Pelayanan Administrasi Pemerintahan')
            ->where('detail_monevs.monev_id', $monev->id)
            ->select(DB::raw('AVG(detail_monevs.rata_rata) AS total'))->first();

        return $total->total;
    }

    /**
     * getRataRataPerIndikator
     *
     * @return array
     */
    public function getRataRataPerIndikator($detailPertanyaan, $indikator)
    {
        $result = 0;
        $arrayIndikator = [];
        foreach ($detailPertanyaan as $pertanyaan) {
            if ($indikator->type == 'Type 1') {
                if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                    $arrayIndikator[$indikator->nama]['type'] = 'Type 1';
                    $arrayIndikator[$indikator->nama]['nilai'] = [
                        'ketersediaan' => [$pertanyaan->bobot_ketersediaan],
                        'kecukupan' => [$pertanyaan->bobot_kecukupan],
                        'kondisi' => [$pertanyaan->bobot_kondisi]
                    ];
                } else {
                    array_push($arrayIndikator[$indikator->nama]['nilai']['ketersediaan'], $pertanyaan->bobot_ketersediaan);
                    array_push($arrayIndikator[$indikator->nama]['nilai']['kecukupan'],  $pertanyaan->bobot_kecukupan);
                    array_push($arrayIndikator[$indikator->nama]['nilai']['kondisi'], $pertanyaan->bobot_kondisi);
                }
            } else if ($indikator->type == 'Type 2') {
                if ($pertanyaan !== null) {
                    if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                        $arrayIndikator[$indikator->nama]['type'] = 'Type 2';
                        $arrayIndikator[$indikator->nama]['nilai']['keb_ket'] = [$pertanyaan->bobot_keb_ket];
                    } else {
                        array_push($arrayIndikator[$indikator->nama]['nilai']['keb_ket'], $pertanyaan->bobot_keb_ket);
                    }
                }
            } else if ($indikator->type == 'Type 3') {
                if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                    $arrayIndikator[$indikator->nama]['type'] = 'Type 3';
                    $arrayIndikator[$indikator->nama]['nilai'] = [
                        [
                            'jum_bobot' => $pertanyaan->jum_bobot_nilai_pendidikan,
                            'banyak' => $pertanyaan->banyak_pendidikan,
                        ]
                    ];
                } else {
                    array_push($arrayIndikator[$indikator->nama]['nilai'], [
                        'jum_bobot' => $pertanyaan->jum_bobot_nilai_pendidikan,
                        'banyak' => $pertanyaan->banyak_pendidikan
                    ]);
                }
            } else if ($indikator->type == 'Type 4') {
                if (!array_key_exists($indikator->nama, $arrayIndikator)) {
                    $arrayIndikator[$indikator->nama]['type'] = 'Type 4';
                    $arrayIndikator[$indikator->nama]['nilai'] = [
                        'ketersediaan' => [$pertanyaan->bobot_ketersediaan]
                    ];
                } else {
                    array_push($arrayIndikator[$indikator->nama]['nilai']['ketersediaan'], $pertanyaan->bobot_ketersediaan);
                }
            }
        }

        $totalPerindikator = [];
        foreach ($arrayIndikator as $key => $avgPerIndikator) {
            $avg1 = [];
            if ($avgPerIndikator['type'] == 'Type 3') {
                $a = [];
                $b = [];
                foreach ($avgPerIndikator['nilai'] as $avg) {
                    array_push($a, $avg['banyak']);
                    array_push($b, $avg['jum_bobot']);
                }
                $jum_bobot = array_sum($b);
                $banyak = array_sum($a);
                $totalPerindikator[$key] = $jum_bobot / $banyak;
            } else {
                foreach ($avgPerIndikator['nilai'] as $avg) {
                    $a = array_sum($avg) / count($avg);
                    array_push($avg1, $a);
                }
                $perIndikator = array_sum($avg1) / count($avg1);
                $totalPerindikator[$key] = $perIndikator;

                if ($avgPerIndikator['type'] == 'Type 2') {
                    dd($avgPerIndikator);
                }
            }
        }

        foreach ($totalPerindikator as $total) {
            return $total;
        };
    }

    public function getJenisLokpris()
    {


        $arrJenisLokpri = array(
            array("value" => "lokpri_batas_darat", "label" => "Lokpri Batas Darat"),
            array("value" => "lokpri_batas_laut", "label" => "Lokpri Batas Laut"),
            array("value" => "lokpri_pksn_darat", "label" => "Lokpri PKSN Darat"),
            array("value" => "lokpri_pksn_laut", "label" => "Lokpri PKSN laut"),
            array("value" => "lokpri_ppkt", "label" => "Lokpri PPKT"),
        );

        $responses = array(
            'Status'    => true,
            'Data'      => $arrJenisLokpri,
            'Message'   => 'Data Jenis Lokpri'
        );
        $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
        return $responses;
    }

    public function getBobotKondisis()
    {

        $bobotKondisis = BobotKondisi::orderBy('id_bobot_kondisi', 'ASC')->get();
        $bobotKondisiArr = array();
        if ($bobotKondisis->count() > 0) {


            foreach ($bobotKondisis as $bobotKondisi) {
                $tmpArr = array(
                    'id'    => $bobotKondisi->id_bobot_kondisi,
                    'nama'  => $bobotKondisi->nama,
                    'bobot'  => $bobotKondisi->bobot,
                );

                array_push($bobotKondisiArr, $tmpArr);
            }
            $responses = array(
                'Status'    => true,
                'Data'      => $bobotKondisiArr,
                'Message'   => 'Data Bobot kondisi'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $responses = array(
                'Status'    => false,
                'Data'      => $bobotKondisiArr,
                'Message'   => 'Data Bobot kondisi'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }



    /**
     * detailReport
     *
     * @return json
     */
    public function detailMonevById($monev_id, Request $request)
    {

        $monev = DataMonevs::find($monev_id);
        $result = [
            'total_rata_rata' => 0,
            'detail_monev' => []
        ];

        if ($monev) {
            $bobotKetersediaan = BobotKetersediaans::first();
            $bobotKondisi = BobotKondisis::first();
            $bobotKecukupan = BobotKecukupans::first();
            $detailMonev = $monev->detail()->get();

            foreach ($detailMonev as $dm) {
                $detailPertanyaan = $dm->detailPertanyaanMonev()->get();
                $dataDetailIndikator = [];
                $inIndikator = [];
                foreach ($detailPertanyaan as $dp) {
                    $dp->pertanyaan = $dp->pertanyaan()->first()->pertanyaan;
                    $indikator = $dp->pertanyaan()->first()->indikator()->first();
                    if ($this->in_array_r($indikator->nama, $dataDetailIndikator)) {
                        $k = array_search($indikator->nama, $inIndikator);
                        // dd($dataDetailIndikator);
                        array_push($dataDetailIndikator[$k]['pertanyaan'], $dp);
                    } else {
                        array_push($inIndikator, $indikator->nama);
                        array_push($dataDetailIndikator, [
                            'indikator' => $indikator->nama,
                            // 'total_rata_rata' => $this->getRataRataPerIndikator($detailPertanyaan, $indikator),
                            'type' => $indikator->type,
                            'pertanyaan' => [$dp]
                        ]);
                    }
                }
                $variabel = $dm->variabel()->first();
                array_push($result['detail_monev'], [
                    'variabel' =>  $variabel,
                    'rata_rata' => $dm->rata_rata,
                    'indikator' => $dataDetailIndikator
                ]);
            }


            $monev = json_decode(json_encode($monev));
            $result['rata_rata_ciq'] = $monev->rata_rata_ciq;
            $result['rata_rata_pap'] = $monev->rata_rata_pap;

            $responses = array(
                'Status'    => true,
                'Data'      => $result,
                'Message'   => 'Data detil monev'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $responses = array(
                'Status'    => false,
                'Data'      => [],
                'Message'   => 'Data tidak ditemukan'
            );
            $responses = response()->json($responses, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }
}
