<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Kecamatan\KecamatanKepegawaian;
use App\Models\Refactored\Kepegawaian\KepegawaianJenisAsn;
use App\Models\Refactored\Kepegawaian\KepegawaianKelembagaan;
use App\Models\Refactored\Kepegawaian\KepegawaianOperasional;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class KecamatanKepegawaianMobileController extends Controller
{
    //
    public function CombosKepegawaian()
    {
        $data['asn'] = KepegawaianJenisAsn::all();
        $data['oper'] = KepegawaianOperasional::all();
        $data['lembaga'] = KepegawaianKelembagaan::all();

        $response_arr = array(
            'Status'    => true,
            'Data'      => $data,
            'Message'   => 'data combo'
        );
        $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
        return $responses;
    }

    public function AddKepegawaianKec(Request $request)
    {
        $input = $request->all();
        // dd($input);
        $valid = Validator::make(
            $input,
            [
                'id' => 'required',
                'asn' => 'required',
                'staf' => 'required',
                'lembaga' => 'required',
                'jumlah' => 'required'
            ],
            [
                'required' => ':attribute harus diisi!',
            ],
            [
                'id' => 'Kecamatan',
                'asn' => 'Jenis ASN',
                'staf' => 'Staf Operasional',
                'lembaga' => 'Kelembagaan',
                'jumlah' => 'Jumlah Pegawai',
            ]
        );

        if (!$valid->fails()) {
            $data_kepeg = [
                'id' => $input['id'],
                'jenis_asn' => $input['asn'],
                'operasional' => $input['staf'],
                'kelembagaan' => $input['lembaga'],
                'jumlah' => (int)$input['jumlah'],
            ];

            $kepeg_exist = KecamatanKepegawaian::where([
                ['id', $input['id']],
                ['jenis_asn', $input['asn']],
                ['operasional', $input['staf']],
                ['kelembagaan', $input['lembaga']]
            ])->first();

            if (!empty($kepeg_exist)) {
                $data_kepeg['jumlah'] += (int)$kepeg_exist['jumlah'];
            }

            DB::beginTransaction();
            try {

                if (empty($kepeg_exist)) {
                    DB::table('kecamatan_kepegawaian')->insert($data_kepeg);
                } else {
                    DB::table('kecamatan_kepegawaian')->where([
                        ['id', $input['id']],
                        ['jenis_asn', $input['asn']],
                        ['operasional', $input['staf']],
                        ['kelembagaan', $input['lembaga']]
                    ])->update([
                        'jumlah' => $data_kepeg['jumlah']
                    ]);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $e) {
                DB::rollback();
                $oke = false;
                dd($e);
            }

            if ($oke) {
                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Data Berhasil ditambahkan'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);

                return $responses;
            } else {
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal menambahkan Pegawai Kecamatan'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);

                return $responses;
            }
        } else {
            return response()->json([
                'status' => 500,
                'msg' => $valid->errors()->first()
            ]);
        }
    }


    public function GetListKepegawaianKecamatan($id)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $prov = KecamatanKepegawaian::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'jenis_asn',
            'operasional',
            'jumlah',
            'kelembagaan',
        ]);
        $prov = $prov->where('id', $id);


        $prov = $prov->get();
        $dataArr = array();
        if ($prov->count() > 0) {
            foreach ($prov as $p) {
                $jenisAsn = KepegawaianJenisAsn::where('jenis_asn', $p->jenis_asn)->first();
                $kepegOps = KepegawaianOperasional::where('idoperasional', $p->operasional)->first();
                $kelembagaan = KepegawaianKelembagaan::where('idkelembagaan', $p->kelembagaan)->first();
                $tmpArr = array(
                    'id'    => $p->id,
                    'jenisAsn'    => !empty($jenisAsn) ? $jenisAsn->keterangan : '',
                    'kepegOps'    => !empty($kepegOps) ? $kepegOps->keterangan : '',
                    'kelembagaan'    => !empty($kelembagaan) ? $kelembagaan->keterangan : '',
                    'jumlah'    => $p->jumlah
                );
                array_push($dataArr, $tmpArr);
            }
            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataArr,
                'Message'   => 'data ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => $dataArr,
                'Message'   => 'data tidak ditemukan'
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }
}
