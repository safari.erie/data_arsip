<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Kecamatan\KecamatanMobilitas;
use App\Models\Refactored\Master\MobilitasItem;
use App\Models\Refactored\Utils\UtilsKecamatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;

class KecamatanMobileMobilitasController extends Controller
{
    //


    private function getLastIdMobilitas()
    {
        $last = MobilitasItem::max('idmobilitas');
        return !empty($last) ? $last + 1 : 1;
    }

    public function AddKecamatanMobilitas(Request $request)
    {
        $input = $request->all();
        // dd($input);
        $valid = Validator::make(
            $input,
            [
                'id' => 'required',
                'nama' => 'required',
                'jumlah' => 'required'
            ],
            [
                'required' => ':attribute harus diisi!',
            ],
            [
                'id' => 'Kecamatan',
                'nama' => 'Nama Item',
                'jumlah' => 'Jumlah Item',
            ]
        );

        if (!$valid->fails()) {
            $exist = MobilitasItem::where('nama', $input['nama'])->first();
            $data_master = [
                'idmobilitas' => @$exist->idmobilitas
            ];

            if (empty($exist)) {
                $idmobilitas = $this->getLastIdMobilitas();
                $data_master = [
                    'idmobilitas' => $idmobilitas,
                    'nama' => $input['nama'],
                    'ishapus' => '0',
                ];
            }

            $data_mobil = [
                'id' => $input['id'],
                'idmobilitas' => $data_master['idmobilitas'],
                'jumlah' => (int)$input['jumlah'],
            ];

            if ($request->has('foto')) {
                $imagename = strtolower('m-' . $input['nama'] . '-kec-' . preg_replace('/\s+/', '', UtilsKecamatan::where('id', $input['id'])->first()->name) . '-' . time() . '.' . $request->foto->extension());
                $request->foto->move(public_path('upload/mobilitas'), $imagename);
                $data_mobil['foto'] = $imagename;
            }

            $mobil_exist = KecamatanMobilitas::where([['id', $input['id']], ['idmobilitas', $data_master['idmobilitas']]])->first();

            if (!empty($mobil_exist)) {
                $data_mobil['jumlah'] += (int)$mobil_exist['jumlah'];
            }

            DB::beginTransaction();
            try {
                if (empty($exist)) {
                    DB::table('mobilitas_item')->insert($data_master);
                }

                if (empty($mobil_exist)) {
                    DB::table('kecamatan_mobilitas')->insert($data_mobil);
                } else {
                    DB::table('kecamatan_mobilitas')->where([['id', $input['id']], ['idmobilitas', $data_master['idmobilitas']]])->update($data_mobil);
                }
                DB::commit();
                $oke = true;
            } catch (\Exception $e) {
                DB::rollback();
                $oke = false;
                dd($e);
            }

            if ($oke) {
                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Data Berhasil ditambahkan'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);

                return $responses;
            } else {
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal menambahkan Mobilitas'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_BAD_REQUEST);

                return $responses;
            }
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => array(),
                'Message'   => $valid->errors()->first()
            );
            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_BAD_REQUEST);

            return $responses;
        }
    }

    public function DataMobilitas($id)
    {
        DB::statement(DB::raw('set @rownum=0'));
        $mobilitasKecamatans = KecamatanMobilitas::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'idmobilitas',
            'jumlah',
            'foto',
        ]);
        //  $mobilitasKecamatans = KecamatanMobilitas::leftJoin('');
        $mobilitasKecamatans = $mobilitasKecamatans->where('id', $id);

        $mobilitasKecamatans = $mobilitasKecamatans->get();

        $dataMobilitasArr = array();
        if ($mobilitasKecamatans->count() > 0) {
            foreach ($mobilitasKecamatans as $r) {
                $name = MobilitasItem::where('idmobilitas', $r->idmobilitas)->first();
                $tmpArr = array(
                    "id"             => $r->id,
                    "idMobilitas"    => $r->idmobilitas,
                    "nama"    => !(empty($name)) ? $name->nama : '',
                    "jumlah"         => number_format($r->jumlah, 0, ',', '.'),
                    "foto"         => $r->foto == null ? '' : asset('upload/mobilitas/' . $r->foto)
                );
                array_push($dataMobilitasArr, $tmpArr);
            }

            $response_arr = array(
                'Status'    => true,
                'Data'      => $dataMobilitasArr,
                'Message'   => 'Data kecamatan  mobilitas ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => $dataMobilitasArr,
                'Message'   => 'Data tidak ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }

    public function getMobilitasById(Request $request)
    {
        $id = $request->id;
        $idmobilitas = $request->idMobilitas;


        $mobil = KecamatanMobilitas::where([['id', $id], ['idmobilitas', $idmobilitas]])->first();
        $mobilArr = array(
            'id'    => $mobil->id,
            'idMobilitas'    => $mobil->idmobilitas,
            'jumlah'    => $mobil->jumlah,
            'foto'    => $mobil->foto == null ? '' : asset('upload/mobilitas/' . $mobil->foto),
        );
        if (!empty($mobil)) {
            $kecamatan = UtilsKecamatan::where('id', $id)->first();
            $data['kecamatan'] = $kecamatan;
            $data['mobilitas'] = $mobilArr;
            $response_arr = array(
                'Status'    => true,
                'Data'      => $data,
                'Message'   => 'Data tidak ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            return $responses;
        } else {
            $response_arr = array(
                'Status'    => false,
                'Data'      => [],
                'Message'   => 'Data tidak ditemukan'
            );

            $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_NOT_FOUND);
            return $responses;
        }
    }

    public function UpdateMobilitasKecamatan(Request $request)
    {
        $input = $request->all();

        $valid = Validator::make(
            $input,
            [
                'id' => 'required',
                'idmobilitas' => 'required',
                'jumlah' => 'required',
            ],
            [
                'required' => ':attribute harus diisi!',
            ],
            [
                'id' => 'Kecamatan',
                'idmobilitas' => 'Mobilitas Item',
                'jumlah' => 'Jumlah Item',
            ]
        );

        if (!$valid->fails()) {
            $data_mobil = [
                'jumlah' => (int)$input['jumlah'],
            ];

            if ($request->has('foto')) {
                $imagename = strtolower('m-' . $input['nama'] . '-kec-' . preg_replace('/\s+/', '', UtilsKecamatan::where('id', $input['id'])->first()->name) . '-' . time() . '.' . $request->foto->extension());
                $request->foto->move(public_path('upload/mobilitas/'), $imagename);
                $data_mobil['foto'] = $imagename;
            }

            DB::beginTransaction();
            try {
                DB::table('kecamatan_mobilitas')->where([['id', $input['id']], ['idmobilitas', $input['idmobilitas']]])->update($data_mobil);
                DB::commit();
                $oke = true;
            } catch (\Exception $e) {
                DB::rollback();
                $oke = false;
                dd($e);
            }

            if ($oke) {
                $response_arr = array(
                    'Status'    => true,
                    'Data'      => array(),
                    'Message'   => 'Data Berhasil diupdate'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
                return $responses;
            } else {
                $response_arr = array(
                    'Status'    => false,
                    'Data'      => array(),
                    'Message'   => 'Gagal mengupdate Mobilitas'
                );
                $responses = response()->json($response_arr, HttpFoundationResponse::HTTP_OK);
            }
        } else {
            return response()->json([
                'status' => 500,
                'msg' => $valid->errors()->first()
            ]);
        }
    }
}
