<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Refactored\Dokumen\Dokumen;
use Illuminate\Http\Request;
use League\CommonMark\Block\Element\Document;
use Symfony\Component\HttpFoundation\Response as HttpFoundationResponse;


class DownloadMobileController extends Controller
{
    //
    public function index()
    {
        $data['dokumen'] = new Dokumen();
        $data['fileExt'] = [
            'pdf' => 'pdf',
            'xls' => 'excel',
            'xlsx' => 'excel',
            'doc' => 'word',
            'docx' => 'word',
            'ppt' => 'powerpoint',
            'pptx' => 'powerpoint',
            'jpg' => 'image',
            'jpeg' => 'image',
            'png' => 'image',
            'mp4' => 'video'
        ];

        if (@request()->cari) {
            $key = request()->cari;
            $data['dokumen'] = $data['dokumen']->whereHas('kategori', function ($q) use ($key) {
                $q->where('keterangan', 'like', '%' . $key . '%');
            })->orWhere('nama', 'like', '%' . $key . '%');
        }

        $dokumens = Dokumen::join('dokumen_kategori as kategori', function ($kategoriJoin) {
            $kategoriJoin->on('dokumen.idkategori', 'kategori.idkategori');
        })->where('ispublic', 1)->orderBy('id', 'desc')->get();

        //$data['dokumens'] = $data['dokumen']->where('ispublic', 1)->orderBy('id', 'desc')->get();
        if (count($dokumens) > 1) {
            $dataDokumen = array();
            foreach ($dokumens as $dokumen) {
                $tmpArr = array(
                    'Id'    => $dokumen->id,
                    'NamaDokumen'   => $dokumen->nama,
                    'Deskripsi' => $dokumen->deskripsi,
                    'Tahun'     => $dokumen->tahun,
                    'Keterangan'    => $dokumen->keterangan
                );
                $dataDokumen[] = $tmpArr;
            }
            $responses = array(
                'status' => true,
                'Data'  => $dataDokumen,
                'Message'   => 'Data tersedia',
            );
        } else {
            $responses = array(
                'status' => false,
                'Data'  => array(),
                'Message'   => 'Data tidak tersedia',
            );
        }

        return $responses;
    }
}
