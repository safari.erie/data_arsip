<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $codeType
 * @property string $code
 * @property string $name
 * @property string $valueString
 * @property string $valueDescription
 */

class ApplSetting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appl_setting';
}
