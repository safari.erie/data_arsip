<?php

namespace App\Models\Refactored\Utils;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string     $name
 */
class UtilsKecamatanLokpri extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'kecamatan_detail_lokpri';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'kecamatanid';



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    // Relations ...
    public function detailLokpri()
    {
        return $this->hasMany(UtilsKecamatan::class, 'kecamatanid', 'kecamatanid');
    }
}
