<?php

namespace App\Models\Refactored\Monev;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BobotKondisi extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bobot_kondisi';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_bobot_kondisi';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'bobot', 'status',  'created', 'updated_at'
    ];
}
