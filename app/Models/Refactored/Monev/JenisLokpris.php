<?php

namespace App\Models\Refactored\Monev;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisLokpris extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jenis_lokpris';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_jenis_lokpri';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama'
    ];
}
