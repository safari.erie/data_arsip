<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\MonevPerorangExport;
use App\Exports\DataPengisiMonevExport;

class MonevSheetsExport implements WithMultipleSheets
{
    use Exportable;

    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [
            new MonevPerorangExport($this->data),
            new DataPengisiMonevExport($this->data),
        ];

        return $sheets;
    }
}
