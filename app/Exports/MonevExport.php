<?php

namespace App\Exports;

/*  */

use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class MonevExport implements FromView, ShouldAutoSize
{
    private $data;
    private $year;

    public function __construct($data, $year)
    {
        $this->data = $data;
        $this->year = $year;
    }

    public function view(): View
    {

        return view('pages.export_monev', [
            'data' => $this->data,
            'year' => $this->year
        ]);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $cellRange = 'A1:M1'; // All headers
                $event->sheet->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
