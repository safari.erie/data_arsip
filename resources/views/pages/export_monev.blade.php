<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Export Monev</title>
</head>

<body>
    <table>
        <tr>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">No</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Nama Pengisi</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Jabatan</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Email</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">No HP</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Alamat Kantor</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Kecamatan</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Kab / Kota</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Provinsi</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Pengawasan CIQ</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Pelayanan Administrasi
                Pemerintah</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Jenis</th>
            <th style="text-align:center; background-color: #abb7b7; border: 1px solid #000000">Tanggal</th>
        </tr>
        @foreach ($data as $key => $value)
            <tr>
                <td style="border: 1px solid #000000">{{ $key + 1 }}</td>
                <td style="border: 1px solid #000000">{{ $value->nama }}</td>
                <td style="border: 1px solid #000000">{{ $value->jabatan }}</td>
                <td style="border: 1px solid #000000">{{ $value->email }}</td>
                <td style="border: 1px solid #000000">{{ (string) $value->no_hp }}</td>
                <td style="border: 1px solid #000000">{{ $value->alamat_kantor }}</td>
                <td style="border: 1px solid #000000">{{ $value->kecamatan }}</td>
                <td style="border: 1px solid #000000">{{ $value->kabupaten }}</td>
                <td style="border: 1px solid #000000">{{ $value->provinsi }}</td>
                <td style="border: 1px solid #000000">{{ $value->rata_rata_ciq }}</td>
                @if ($value->rata_rata_pap >= 0 && $value->rata_rata_pap < 50)
                    <td style="border: 1px solid #000000; background-color: #fd3995; color:#ffffff" align="center">
                        <b>{{ $value->rata_rata_pap }}</b>
                    </td>
                @elseif($value->rata_rata_pap >= 50 && $value->rata_rata_pap < 80)
                    <td style="border: 1px solid #000000; background-color: #ffb822; color:black" align="center">
                        <b>{{ $value->rata_rata_pap }}</b>
                    </td>
                @elseif($value->rata_rata_pap >= 80 && $value->rata_rata_pap <= 100)
                    <td style="border: 1px solid #000000; background-color: #34bfa3; color:#ffffff" align="center">
                        <b>{{ $value->rata_rata_pap }}</b>
                    </td>
                @endif
                @if ($value->lokpri_batas_darat == 1)
                    <td>LOKPRI Batas Darat</td>
                @elseif ($value->lokpri_batas_laut == 1)
                    <td>LOKPRI Batas Laut</td>
                @elseif ($value->lokpri_pksn_darat == 1)
                    <td>LOKPRI PKSN Darat</td>
                @elseif ($value->lokpri_pksn_laut == 1)
                    <td>LOKPRI PKSN Laut</td>
                @elseif ($value->lokpri_ppkt == 1)
                    <td>LOKPRI PPKT</td>
                @endif
                <td style="border: 1px solid #000000">{{ $value->updated_at }}</td>
            </tr>
        @endforeach
    </table>
</body>

</html>
