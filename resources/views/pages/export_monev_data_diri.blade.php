<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Diri Pengisi</title>
</head>

<body>
    <table>
        <tr>
            <td style="border: 1px solid #000000;">Nama</td>
            <td style="border: 1px solid #000000;">{{ $data->nama }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">Jabatan</td>
            <td style="border: 1px solid #000000;">{{ $data->jabatan }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">Email</td>
            <td style="border: 1px solid #000000;">{{ $data->email }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">No HP</td>
            <td style="border: 1px solid #000000;">{{ (string) $data->no_hp }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">Alamat Kantor</td>
            <td style="border: 1px solid #000000;">{{ $data->alamat_kantor }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">Kecamatan</td>
            <td style="border: 1px solid #000000;">{{ $data->kecamatan }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">Kabupaten / Kota</td>
            <td style="border: 1px solid #000000;">{{ $data->kabupaten }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">Provinsi</td>
            <td style="border: 1px solid #000000;">{{ $data->provinsi }}</td>
        </tr>
        <tr>
            <td style="border: 1px solid #000000;">
                Rata-rata CIQ
            </td>
            <td style="border: 1px solid #000000;">{{ $data->rata_rata_ciq }}</td>
        </tr>
        @if ($data->lokpri_batas_darat == 1)
            <tr>
                <td style="border: 1px solid #000000;">Jenis</td>
                <td style="border: 1px solid #000000;">LOKPRI Batas Darat</td>
            </tr>
        @elseif ($data->lokpri_batas_laut == 1)
            <tr>
                <td style="border: 1px solid #000000;">Jenis</td>
                <td style="border: 1px solid #000000;">LOKPRI Batas Laut</td>
            </tr>
        @elseif ($data->lokpri_pksn_darat == 1)
            <tr>
                <td style="border: 1px solid #000000;">Jenis</td>
                <td style="border: 1px solid #000000;">LOKPRI PKSN Darat</td>
            </tr>
        @elseif ($data->lokpri_pksn_laut == 1)
            <tr>
                <td style="border: 1px solid #000000;">Jenis</td>
                <td style="border: 1px solid #000000;">LOKPRI PKSN Darat</td>
            </tr>
        @elseif ($data->lokpri_ppkt == 1)
            <tr>
                <td style="border: 1px solid #000000;">Jenis</td>
                <td style="border: 1px solid #000000;">LOKPRI PPKT</td>
            </tr>
        @endif
        <tr>
            <td style="border: 1px solid #000000;">
                Rata-rata PAP
            </td>
            @if ($data->rata_rata_pap >= 0 && $data->rata_rata_pap < 50)
                <td style="border: 1px solid #000000; background-color: #fd3995; color:#ffffff" align="center">
                    <b>{{ $data->rata_rata_pap }}</b>
                </td>
            @elseif($data->rata_rata_pap >= 50 && $data->rata_rata_pap < 80)
                <td style="border: 1px solid #000000; background-color: #ffb822; color:black" align="center">
                    <b>{{ $data->rata_rata_pap }}</b>
                </td>
            @elseif($data->rata_rata_pap >= 80 && $data->rata_rata_pap <= 100)
                <td style="border: 1px solid #000000; background-color: #34bfa3; color:#ffffff" align="center">
                    <b>{{ $data->rata_rata_pap }}</b>
                </td>
            @endif
        </tr>
    </table>
</body>

</html>
