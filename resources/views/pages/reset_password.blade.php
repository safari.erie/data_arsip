<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reset Password</title>
    <style>
        .btn {
            width: 450px;
            background-color: #5995fd;
            border: none;
            outline: none;
            height: 49px;
            border-radius: 49px;
            color: #fff;
            text-transform: uppercase;
            font-weight: 600;
            margin: 10px 0;
            cursor: pointer;
            transition: 0.5s;
            text-decoration: none;
        }
    </style>
</head>

<body>
    <p> Password telah di rubah menjad <strong> {{ $details['password_reset'] }}</strong> silahkan change password
        untuk keamanan
        ....!!!</p>

</body>

</html>
