<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Bukti Monev</title>
</head>

<body>
    <table>
        @foreach ($data->detail_monev as $key => $value)
            <tr>
                <th style="background-color:#fd3995" colspan="5">{{ $value->variabel->nama }}
                    ({{ $value->variabel->presentase }}%)</th>
                <th>Total Rata-Rata : {{ $value->rata_rata }}</th>
            </tr>
            @foreach ($value->indikator as $k_indikator => $indikator)
                <tr>
                    <th style="background-color:#ffb822" colspan="5">{{ $indikator->indikator }}</th>
                </tr>
                @if ($indikator->type == 'Type 1')
                    <tr>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">No</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Indikator Evaluasi</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Ketersediaan</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Kecukupan</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Kelayakan</th>
                    </tr>
                    @foreach ($indikator->pertanyaan as $k_pertanyaan => $pertanyaan)
                        <tr>
                            <td style="border: 1px solid #000000;">{{ $k_pertanyaan + 1 }}</td>
                            <td style="border: 1px solid #000000;">{{ $pertanyaan->pertanyaan }}</td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->ketersediaan !== null ? $pertanyaan->ketersediaan : 'Tidak Ada' }}
                            </td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->kecukupan !== null ? $pertanyaan->kecukupan : 'Tidak Ada' }}
                            </td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->kondisi !== null ? $pertanyaan->kondisi : 'Tidak Ada' }}
                            </td>
                        </tr>
                    @endforeach
                @elseif ($indikator->type == 'Type 2')
                    <tr>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">No</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Indikator Evaluasi</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Kebutuhan</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Keterisian</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Bobot Nilai</th>
                    </tr>
                    @foreach ($indikator->pertanyaan as $k_pertanyaan => $pertanyaan)
                        <tr>
                            <td style="border: 1px solid #000000;">{{ $k_pertanyaan + 1 }}</td>
                            <td style="border: 1px solid #000000;">{{ $pertanyaan->pertanyaan }}</td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->kebutuhan !== null ? $pertanyaan->kebutuhan : 'Tidak Ada' }}
                            </td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->keterisian !== null ? $pertanyaan->keterisian : 'Tidak Ada' }}
                            </td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->bobot_keb_ket !== null ? $pertanyaan->bobot_keb_ket : 'Tidak Ada' }}
                            </td>
                        </tr>
                    @endforeach
                @elseif ($indikator->type == 'Type 3')
                    <tr>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">No</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Indikator Evaluasi</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Bobot Nilai</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Banyaknya</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Jumlah Bobot Nilai</th>
                    </tr>
                    @foreach ($indikator->pertanyaan as $k_pertanyaan => $pertanyaan)
                        <tr>
                            <td style="border: 1px solid #000000;">{{ $k_pertanyaan + 1 }}</td>
                            <td style="border: 1px solid #000000;">{{ $pertanyaan->pertanyaan }}</td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->bobot_nilai_pendidikan !== null ? $pertanyaan->bobot_nilai_pendidikan : 'Tidak Ada' }}
                            </td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->banyak_pendidikan !== null ? $pertanyaan->banyak_pendidikan : 'Tidak Ada' }}
                            </td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->jum_bobot_nilai_pendidikan !== null ? $pertanyaan->jum_bobot_nilai_pendidikan : 'Tidak Ada' }}
                            </td>
                        </tr>
                    @endforeach
                @elseif ($indikator->type == 'Type 4')
                    <tr>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">No</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Indikator Evaluasi</th>
                        <th style="border: 1px solid #000000; background-color:#34bfa3">Ketersediaan</th>
                    </tr>
                    @foreach ($indikator->pertanyaan as $k_pertanyaan => $pertanyaan)
                        <tr>
                            <td style="border: 1px solid #000000;">{{ $k_pertanyaan + 1 }}</td>
                            <td style="border: 1px solid #000000;">{{ $pertanyaan->pertanyaan }}</td>
                            <td style="border: 1px solid #000000;">
                                {{ $pertanyaan->ketersediaan !== null ? $pertanyaan->ketersediaan : 'Tidak Ada' }}
                            </td>
                        </tr>
                    @endforeach
                @endif
                <tr></tr>
            @endforeach
            <tr></tr>
        @endforeach
        <tr></tr>
    </table>
</body>

</html>
